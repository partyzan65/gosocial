CREATE TABLE messages
(
    user_id    BIGINT(20) UNSIGNED NOT NULL,
    from_id    BIGINT(20) UNSIGNED NOT NULL,
    to_id      BIGINT(20) UNSIGNED NOT NULL,
    region_id  INT UNSIGNED        NOT NULL,
    dt_created BIGINT(20) UNSIGNED NOT NULL,
    has_read   BOOLEAN DEFAULT FALSE,
    message    VARCHAR(20000)      NOT NULL,
    PRIMARY KEY (user_id, from_id, to_id, region_id, dt_created)
) ENGINE = InnoDB;
