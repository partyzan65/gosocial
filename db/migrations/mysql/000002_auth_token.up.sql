CREATE TABLE auth_token (
  id BIGINT(20) NOT NULL AUTO_INCREMENT,
  user_id BIGINT(20) NOT NULL,
  token VARCHAR(64) NOT NULL,
  type ENUM('auth') NOT NULL,
  dt_expired DATETIME NOT NULL,
  dt_created DATETIME NOT NULL DEFAULT NOW(),
  PRIMARY KEY (id),
  UNIQUE (token),
  CONSTRAINT fk_auth_token_user_id FOREIGN KEY (user_id) REFERENCES `user` (id)
) ENGINE = InnoDB CHARACTER SET utf8mb4;
