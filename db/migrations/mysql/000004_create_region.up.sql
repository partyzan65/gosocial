CREATE TABLE region
(
    id   INT         NOT NULL AUTO_INCREMENT,
    name VARCHAR(64) NOT NULL,
    PRIMARY KEY (id)
) ENGINE = InnoDB
  CHARACTER SET utf8mb4;

INSERT INTO region (id, name)
VALUES (101, 'Russia'),
       (202, 'USA'),
       (203, 'United Kingdom'),
       (104, 'Germany'),
       (105, 'China'),
       (205, 'Australia');


ALTER TABLE user
    ADD region_id INT NOT NULL;

UPDATE user SET region_id = 101 WHERE 1;

ALTER TABLE user
    ADD CONSTRAINT fk_region_id FOREIGN KEY (region_id) REFERENCES region(id);

