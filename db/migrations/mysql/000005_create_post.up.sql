CREATE TABLE post
(
    id         BIGINT(20)    NOT NULL AUTO_INCREMENT,
    user_id    BIGINT(20)    NOT NULL,
    name       VARCHAR(1000) NOT NULL,
    image      VARCHAR(255)  NOT NULL,
    text       TEXT          NOT NULL,
    dt_created DATETIME      NOT NULL DEFAULT NOW(),
    dt_updated DATETIME      NOT NULL DEFAULT NOW(),
    PRIMARY KEY (id),
    CONSTRAINT fk_post_user_id FOREIGN KEY (user_id) REFERENCES `user` (id)
) ENGINE = InnoDB
  CHARACTER SET utf8mb4;

CREATE TABLE subscription
(
    user_id     BIGINT(20) NOT NULL,
    subscriber_id BIGINT(20) NOT NULL,
    PRIMARY KEY (user_id, subscriber_id),
    CONSTRAINT fk_follower_user_id FOREIGN KEY (user_id) REFERENCES `user` (id),
    CONSTRAINT fk_follower_id FOREIGN KEY (subscriber_id) REFERENCES `user` (id)
) ENGINE = InnoDB
  CHARACTER SET utf8mb4;