ALTER TABLE user
    DROP FOREIGN KEY fk_region_id;

ALTER TABLE user
    DROP COLUMN region_id;

DROP TABLE region;
