CREATE TABLE user
(
    id             BIGINT(20)                        NOT NULL AUTO_INCREMENT,
    login          VARCHAR(128)                      NOT NULL,
    password       VARCHAR(64)                       NOT NULL,
    status         ENUM ('new', 'active', 'blocked') NOT NULL DEFAULT 'new',
    first_name     VARCHAR(255)                      NOT NULL,
    last_name      VARCHAR(255)                      NOT NULL,
    bdate          DATE                              NOT NULL,
    gender         ENUM ('male', 'female')           NOT NULL,
    description    TEXT                              NOT NULL,
    city           VARCHAR(64)                                DEFAULT NULL,
    dt_created     DATETIME                          NOT NULL DEFAULT NOW(),
    dt_updated     DATETIME                          NOT NULL,
    dt_last_logged DATETIME                          NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (login)
) ENGINE=InnoDB CHARACTER SET utf8mb4;
