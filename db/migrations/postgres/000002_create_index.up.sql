CREATE EXTENSION IF NOT EXISTS pg_trgm;

CREATE INDEX idx_user_first_name
    ON public.user USING gin (first_name gin_trgm_ops);
CREATE INDEX idx_user_last_name
    ON public.user USING gin (last_name gin_trgm_ops);