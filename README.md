# gosocial

Требуется разработать создание и просмотр анект в социальной сети.

Функциональные требования:
- Авторизация по паролю.
- Страница регистрации, где указывается следующая информация:
- Имя
- Фамилия
- Возраст
- Пол
- Интересы
- Город
- Страницы с анкетой.

Нефункциональные требования:
- Любой язык программирования
- В качестве базы данных использовать MySQL
- Не использовать ORM
- Программа должна представлять из себя монолитное приложение.
- Не рекомендуется использовать следующие технологии:
  - Контейнеры
  - Репликация
  - Шардинг
  - Индексы
  - Кэширование

### Миграции
[golang-migrate](https://github.com/golang-migrate/migrate)
```bash
migrate create -ext sql -dir migrations/mysql -seq create_region

export MYSQL_URL="mysql://user:password@tcp(host:port)/dbname"

# up
migrate -database ${MYSQL_URL} -path migrations/mysql up

# down
migrate -database ${MYSQL_URL} -path migrations/mysql down 1
```

