package repository_test

import (
	"context"
	"testing"

	"github.com/partyzanex/testutils"
	"gitlab.com/partyzan65/gosocial/pkg/region/repository"
)

func TestRepository_Search(t *testing.T) {
	db := testutils.NewSqlDB(t, "mysql", "MYSQL_TEST")

	repo := repository.New(db)

	ctx := context.Background()

	regions, err := repo.Search(ctx)
	testutils.Err(t, "repo.Search(ctx)", err)
	testutils.AssertEqual(t, "regions", 5, len(regions))

	regions, err = repo.Search(ctx, 1, 2)
	testutils.Err(t, "repo.Search(ctx, 1, 2)", err)
	testutils.AssertEqual(t, "regions", 2, len(regions))
}
