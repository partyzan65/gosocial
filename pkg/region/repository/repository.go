package repository

import (
	"context"
	"database/sql"
	"strconv"
	"strings"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"gitlab.com/partyzan65/gosocial/pkg/entity"
	"gitlab.com/partyzan65/gosocial/pkg/executor"
)

const (
	selectQuery = `select id, name from region`
)

type repository struct {
	ex executor.Executor
}

func New(db *sql.DB) *repository {
	return &repository{
		ex: sqlx.NewDb(db, "mysql"),
	}
}

func (repo *repository) Search(ctx context.Context, ids ...int) ([]*entity.Region, error) {
	where := " where 1"

	if n := len(ids); n > 0 {
		regionIDs := make([]string, n)
		for i, id := range ids {
			regionIDs[i] = strconv.Itoa(id)
		}

		where += ` and id in (` + strings.Join(regionIDs, ",") + `)`
	}

	_, ex := executor.FromContext(ctx, repo.ex)

	var regions []*entity.Region
	query := selectQuery + where

	err := ex.Select(&regions, query)
	if err != nil {
		return nil, errors.Wrap(err, "search for regions failed")
	}

	return regions, nil
}
