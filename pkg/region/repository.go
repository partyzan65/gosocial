package region

import (
	"context"
	"gitlab.com/partyzan65/gosocial/pkg/entity"
)

type Repository interface {
	Search(ctx context.Context, ids ...int) ([]*entity.Region, error)
}
