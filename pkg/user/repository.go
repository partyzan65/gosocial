package user

import (
	"context"

	"gitlab.com/partyzan65/gosocial/pkg/entity"
)

type Filter struct {
	Status        entity.UserStatus
	Login         string
	Name          string
	Limit, Offset int
	NotCount      bool
	IDs           []int64
}

type Repository interface {
	Search(ctx context.Context, filter *Filter) ([]*entity.User, int, error)
	Create(ctx context.Context, user entity.User) (*entity.User, error)
	Update(ctx context.Context, user entity.User) (*entity.User, error)
	Delete(ctx context.Context, user entity.User) error
}
