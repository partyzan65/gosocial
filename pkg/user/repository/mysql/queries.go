package mysql

const (
	selectQuery = `
select 
	id, first_name, last_name, login, password, status, 
	bdate, gender, description, city, 1 as password_is_encoded, 
	dt_created, dt_updated, dt_last_logged, region_id
from user
%s
order by %s
limit %d offset %d
`
	countQuery  = `select count(*) as cnt from user %s`
	insertQuery = `
insert into user (
	first_name, last_name, login, password, status, bdate, gender, description, city, 
	dt_created, dt_updated, dt_last_logged, region_id
) values (
	:first_name, :last_name, :login, :password, :status, :bdate, :gender, :description, :city, 
	:dt_created, :dt_updated, :dt_last_logged, :region_id
)
`
	updateQuery = `update user set %s where id = :id`
	deleteQuery = `delete from user where id = ?`
)
