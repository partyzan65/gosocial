package mysql_test

import (
	"context"
	"testing"
	"time"

	"github.com/asticode/go-astitools/string"
	"github.com/partyzanex/testutils"
	"gitlab.com/partyzan65/gosocial/pkg/datetime"
	"gitlab.com/partyzan65/gosocial/pkg/entity"
	"gitlab.com/partyzan65/gosocial/pkg/user/repository/mysql"
)

func TestRepository_Create(t *testing.T) {
	db := testutils.NewSqlDB(t, "mysql", "MYSQL_TEST")

	repo := mysql.New(db)

	exp := getTestUser()

	ctx := context.Background()

	got, err := repo.Create(ctx, *exp)
	testutils.FatalErr(t, "repo.Create", err)

	exp.ID = got.ID
	exp.DTCreated = got.DTCreated
	exp.DTUpdated = got.DTUpdated
	exp.DTLastLogged = got.DTLastLogged

	equalUser(t, "repo.Create", exp, got)

	//got, err = repo.SearchByID(ctx, exp.ID)
	//testutils.FatalErr(t, "repo.SearchByID", err)

	//equalUser(t, "repo.SearchByID", exp, got)

	//got, err = repo.SearchByLogin(ctx, exp.Login)
	//testutil.FatalErr(t, "repo.SearchByLogin", err)
	//
	//equalUser(t, "repo.SearchByLogin", exp, got)

	exp.FirstName = astistring.RandomString(20)
	exp.LastName = astistring.RandomString(20)
	exp.Description = astistring.RandomString(2022)
	exp.City = astistring.RandomString(12)
	exp.Gender = entity.Male
	exp.Login = astistring.RandomString(30)
	exp.Status = entity.UserActive
	exp.Password = astistring.RandomString(64)
	exp.DTLastLogged = time.Now()
	exp.RegionID = 205

	got, err = repo.Update(ctx, *exp)
	testutils.FatalErr(t, "repo.Update", err)

	exp.DTUpdated = got.DTUpdated

	equalUser(t, "repo.Update", exp, got)

	err = repo.Delete(ctx, *exp)
	testutils.FatalErr(t, "repo.Delete", err)

	//_, err = repo.SearchByID(ctx, exp.ID)
	//if err != domain.ErrUserNotFound {
	//	t.Fatalf("expected error, got %v", err)
	//}
}

func equalUser(t *testing.T, op string, exp, got *entity.User) {
	t.Log("start equal for", op)

	testutils.AssertEqual(t, "ID", exp.ID, got.ID)
	testutils.AssertEqual(t, "Login", exp.Login, got.Login)
	testutils.AssertEqual(t, "Status", exp.Status, got.Status)
	testutils.AssertEqual(t, "DTCreated", exp.DTCreated.Unix(), got.DTCreated.Unix())
	testutils.AssertEqual(t, "DTUpdated", exp.DTUpdated.Unix(), got.DTUpdated.Unix())
	testutils.AssertEqual(t, "DTLastLogged", exp.DTLastLogged.Unix(), got.DTLastLogged.Unix())
	testutils.AssertEqual(t, "FirstName", exp.FirstName, got.FirstName)
	testutils.AssertEqual(t, "LastName", exp.LastName, got.LastName)
	testutils.AssertEqual(t, "BDate", time.Time(exp.BDate).Format("20060102"), time.Time(got.BDate).Format("20060102"))
	testutils.AssertEqual(t, "Gender", exp.Gender, got.Gender)
	testutils.AssertEqual(t, "Description", exp.Description, got.Description)
	testutils.AssertEqual(t, "City", exp.City, got.City)
}

func getTestUser() *entity.User {
	return &entity.User{
		Login:       astistring.RandomString(24),
		Password:    astistring.RandomString(64),
		Status:      entity.UserNew,
		RegionID:    101,
		FirstName:   astistring.RandomString(18),
		LastName:    astistring.RandomString(18),
		Gender:      entity.Female,
		BDate:       datetime.Date(time.Now()),
		City:        astistring.RandomString(30),
		Description: astistring.RandomString(3992),
	}
}
