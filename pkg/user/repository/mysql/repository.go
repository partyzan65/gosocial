package mysql

import (
	"context"
	"database/sql"
	"fmt"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"gitlab.com/partyzan65/gosocial/pkg/entity"
	"gitlab.com/partyzan65/gosocial/pkg/executor"
	"gitlab.com/partyzan65/gosocial/pkg/user"
)

const (
	defaultLimit = 200
	orderBy      = "id asc"
)

type Repository struct {
	db *sqlx.DB
}

type counter struct {
	Count int `db:"cnt"`
}

func (repo *Repository) Search(ctx context.Context, filter *user.Filter) ([]*entity.User, int, error) {
	var users []*entity.User
	var count counter

	limit, offset, delta := defaultLimit, 0, 2
	order := orderBy
	where := `where 1`
	var args []interface{}

	if filter != nil {
		if filter.Limit > 0 {
			limit = filter.Limit
		}
		if filter.Offset > 0 {
			offset = filter.Offset
		}

		if filter.Login != "" {
			args = append(args, filter.Login)
			where += ` and login = ?`
		}
		if filter.Name != "" {
			clause := filter.Name + "%"
			args = append(args, clause, clause)
			where += ` and (first_name like ? or last_name like ?)`
		}

		if filter.Status.IsValid() {
			args = append(args, filter.Status)
			where += " and `status` = ?"
		}

		if n := len(filter.IDs); n > 0 {
			ids := make([]string, n)
			for i, id := range filter.IDs {
				ids[i] = strconv.FormatInt(id, 10)
			}

			where += ` and id in (` + strings.Join(ids, ",") + ")"
		}

		if filter.NotCount {
			delta = 1
		}
	}

	_, ex := executor.FromContext(ctx, repo.db)

	var err1, err2 error

	wg := &sync.WaitGroup{}
	wg.Add(delta)

	go func() {
		defer wg.Done()
		query := fmt.Sprintf(selectQuery, where, order, limit, offset)
		err1 = ex.Select(&users, query, args...)
	}()

	if delta > 1 {
		go func() {
			defer wg.Done()
			query := fmt.Sprintf(countQuery, where)
			err2 = ex.Get(&count, query, args...)
		}()
	}

	wg.Wait()

	if err1 != nil && err1 != sql.ErrNoRows {
		return nil, count.Count, errors.Wrap(err1, "search for users failed")
	}
	if err2 != nil {
		return nil, count.Count, errors.Wrap(err2, "getting count of users failed")
	}

	return users, count.Count, nil
}

func (repo *Repository) Create(ctx context.Context, user entity.User) (*entity.User, error) {
	var tx *sqlx.Tx
	var err error

	tx, err = repo.db.Beginx()
	if err != nil {
		return nil, errors.Wrap(err, "crating transaction failed")
	}
	defer func() {
		if err == nil {
			err = tx.Commit()
			return
		}

		err = tx.Rollback()
	}()

	stmt, err := tx.PrepareNamed(insertQuery)
	if err != nil {
		return nil, errors.Wrap(err, "preparing named statement failed")
	}

	user.DTCreated = time.Now()
	user.DTUpdated = user.DTCreated
	user.DTLastLogged = user.DTCreated

	res, err := stmt.Exec(&user)
	if err != nil {
		return nil, errors.Wrap(err, "executing query failed")
	}

	user.ID, err = res.LastInsertId()
	if err != nil {
		return nil, errors.Wrap(err, "getting last insert id failed")
	}

	return &user, err
}

func (repo *Repository) Update(ctx context.Context, user entity.User) (*entity.User, error) {
	var tx *sqlx.Tx
	var err error

	tx, err = repo.db.Beginx()
	if err != nil {
		return nil, errors.Wrap(err, "crating transaction failed")
	}
	defer func() {
		if err == nil {
			err = tx.Commit()
			return
		}

		err = tx.Rollback()
	}()

	fields := []string{
		"first_name = :first_name", "last_name = :last_name", "login = :login", "status = :status",
		"bdate = :bdate", "gender = :gender", "description = :description", "city = :city",
		"dt_updated = :dt_updated", "dt_last_logged = :dt_last_logged",
	}

	if user.Password != "" {
		fields = append(fields, "password = :password")
	}

	query := fmt.Sprintf(updateQuery, strings.Join(fields, ", "))

	stmt, err := tx.PrepareNamed(query)
	if err != nil {
		return nil, errors.Wrap(err, "preparing named statement failed")
	}

	user.DTUpdated = time.Now()

	_, err = stmt.Exec(&user)
	if err != nil {
		return nil, errors.Wrap(err, "executing query failed")
	}

	return &user, err
}

func (repo *Repository) Delete(ctx context.Context, user entity.User) error {
	var tx *sqlx.Tx
	var err error

	tx, err = repo.db.Beginx()
	if err != nil {
		return errors.Wrap(err, "crating transaction failed")
	}
	defer func() {
		if err == nil {
			err = tx.Commit()
			return
		}

		err = tx.Rollback()
	}()

	_, err = tx.Exec(deleteQuery, user.ID)
	if err != nil {
		return errors.Wrap(err, "deleting user failed")
	}

	return err
}

func New(db *sql.DB) *Repository {
	return &Repository{
		db: sqlx.NewDb(db, "mysql"),
	}
}
