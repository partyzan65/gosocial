package user

import (
	"context"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/partyzan65/gosocial/pkg/entity"
	"gitlab.com/partyzan65/gosocial/pkg/region"
	"golang.org/x/crypto/bcrypt"
)

type UseCase struct {
	Repository

	Regions region.Repository
}

func (uc *UseCase) SearchByLogin(ctx context.Context, login string) (*entity.User, error) {
	users, _, err := uc.Repository.Search(ctx, &Filter{
		Limit:    1,
		Offset:   0,
		Login:    login,
		NotCount: true,
	})
	if err != nil {
		return nil, err
	}

	if users == nil {
		return nil, ErrUserNotFound
	}

	return users[0], nil
}

func (uc *UseCase) SearchByID(ctx context.Context, id int64) (*entity.User, error) {
	users, _, err := uc.Repository.Search(ctx, &Filter{
		Limit:    1,
		Offset:   0,
		IDs:      []int64{id},
		NotCount: true,
	})
	if err != nil {
		return nil, err
	}

	if users == nil {
		return nil, ErrUserNotFound
	}

	return users[0], nil
}

func (uc *UseCase) SetLastLogged(ctx context.Context, user *entity.User) error {
	user.DTLastLogged = time.Now()

	_, err := uc.Repository.Update(ctx, *user)
	if err != nil {
		return err
	}

	return nil
}

func (uc *UseCase) Register(ctx context.Context, user *entity.User) error {
	if err := uc.Validate(user, true); err != nil {
		return err
	}

	_, err := uc.Repository.Create(ctx, *user)
	if err != nil {
		return err
	}

	return nil
}

func (uc *UseCase) Validate(u *entity.User, create bool) error {
	if !create {
		if u.ID == 0 {
			return ErrRequiredUserID
		}
	}

	if create {
		if u.Password == "" {
			return ErrRequiredUserPassword
		}

		if _, err := uc.EncodePassword(u); err != nil {
			return err
		}
	}

	if u.FirstName == "" {
		return ErrRequiredFirstName
	}

	if u.LastName == "" {
		return ErrRequiredLastName
	}

	if u.Login == "" {
		return ErrRequiredUserLogin
	}

	if !u.Status.IsValid() {
		return ErrInvalidUserStatus
	}

	if !u.Gender.IsValid() {
		return ErrInvalidUserGender
	}

	if u.RegionID == 0 {
		return ErrRequiredRegion
	}

	return nil
}

func (uc *UseCase) EncodePassword(user *entity.User) (string, error) {
	if user.PasswordIsEncoded {
		return user.Password, nil
	}

	p, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.MinCost)
	if err != nil {
		return "", errors.Wrap(err, "encoding password failed")
	}

	user.PasswordIsEncoded = true
	user.Password = string(p)

	return user.Password, nil
}

func (uc *UseCase) ComparePassword(user *entity.User, password string) (bool, error) {
	p, err := uc.EncodePassword(user)
	if err != nil {
		return false, err
	}

	err = bcrypt.CompareHashAndPassword([]byte(p), []byte(password))
	return err == nil, err
}

func (uc *UseCase) BindRegions(ctx context.Context, users ...*entity.User) error {
	var regionIDs []int
	ids := make(map[int]bool)

	for _, user := range users {
		if user == nil {
			continue
		}

		if _, ok := ids[user.RegionID]; !ok {
			regionIDs = append(regionIDs, user.RegionID)
		}
	}

	regions, err := uc.Regions.Search(ctx, regionIDs...)
	if err != nil {
		return err
	}

	regionsByID := make(map[int]*entity.Region, len(regions))

	for _, r := range regions {
		regionsByID[r.ID] = r
	}

	for _, user := range users {
		if user == nil {
			continue
		}

		if r, ok := regionsByID[user.RegionID]; ok {
			user.Region = r
		}
	}

	return nil
}

func NewUseCase(users Repository, regions region.Repository) *UseCase {
	return &UseCase{
		Repository: users,
		Regions:    regions,
	}
}
