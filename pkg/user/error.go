package user

import "github.com/pkg/errors"

var (
	ErrUserNotFound         = errors.New("user not found")
	ErrRequiredUserID       = errors.New("required user id")
	ErrRequiredFirstName    = errors.New("required first name")
	ErrRequiredLastName     = errors.New("required last name")
	ErrRequiredUserLogin    = errors.New("required user login")
	ErrRequiredUserPassword = errors.New("required user password")
	ErrInvalidUserStatus    = errors.New("invalid user status")
	ErrInvalidUserGender    = errors.New("invalid user gender")
	ErrRequiredRegion       = errors.New("required user region")
)
