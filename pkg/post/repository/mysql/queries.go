package mysql

const (
	selectQuery = `
select 
	id, user_id, name, image, text, dt_created, dt_updated
from post
%s
order by %s
limit %d offset %d
`
	countQuery  = `select count(*) from post %s`
	insertQuery = `
insert into post (
	user_id, name, image, text, dt_created, dt_updated
) values (
	:user_id, :name, :image, :text, :dt_created, :dt_updated
)
`
	updateQuery = `update post set %s where id = :id`
	deleteQuery = `delete from post where id = ?`
)
