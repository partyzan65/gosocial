package mysql_test

import (
	"context"
	"github.com/labstack/gommon/random"
	"github.com/partyzanex/testutils"
	"gitlab.com/partyzan65/gosocial/pkg/datetime"
	"gitlab.com/partyzan65/gosocial/pkg/entity"
	"gitlab.com/partyzan65/gosocial/pkg/post/repository/mysql"
	usersRepo "gitlab.com/partyzan65/gosocial/pkg/user/repository/mysql"
	"testing"
	"time"
)

func TestNew(t *testing.T) {
	db := testutils.NewSqlDB(t, "mysql", "MYSQL_TEST")

	ctx := context.Background()

	repo := mysql.New(db)
	users := usersRepo.New(db)

	user, err := users.Create(ctx, entity.User{
		ID:                testutils.RandInt64(9999999, 99999999),
		Login:             random.String(50),
		Password:          "",
		Status:            entity.UserBlocked,
		FirstName:         random.String(20),
		LastName:          random.String(20),
		BDate:             datetime.Date(time.Now().AddDate(-20, 0, 0)),
		Gender:            entity.Female,
		Description:       random.String(200),
		RegionID:          101,
		City:              random.String(20),
		PasswordIsEncoded: true,
		Current:           false,
	})
	testutils.FatalErr(t, "users.Create", err)

	defer func() {
		err := users.Delete(ctx, *user)
		testutils.Err(t, "users.Delete(ctx, *user)", err)
	}()

	exp := getTestPost(user)

	got, err := repo.Create(ctx, *exp)
	testutils.FatalErr(t, "repo.Create", err)

	exp.ID = got.ID
	exp.DTCreated = got.DTCreated
	exp.DTUpdated = got.DTUpdated

	equalPost(t, exp, got)

	exp.Text += random.String(50)
	exp.Name += random.String(10)
	exp.Image += random.String(15)

	got, err = repo.Update(ctx, *exp)
	testutils.FatalErr(t, "repo.Update", err)

	exp.DTUpdated = got.DTUpdated

	equalPost(t, exp, got)

	err = repo.Delete(ctx, *exp)
	testutils.FatalErr(t, "repo.Delete", err)
}

func equalPost(t testutils.Tester, exp, got *entity.Post) {
	testutils.AssertEqual(t, "ID", exp.ID, got.ID)
	testutils.AssertEqual(t, "Name", exp.Name, got.Name)
	testutils.AssertEqual(t, "Image", exp.Image, got.Image)
	testutils.AssertEqual(t, "Text", exp.Text, got.Text)
	testutils.AssertEqual(t, "UserID", exp.UserID, got.UserID)
	testutils.AssertEqual(t, "DTCreated", exp.DTCreated.String(), got.DTCreated.String())
	testutils.AssertEqual(t, "DTUpdated", exp.DTUpdated.String(), got.DTUpdated.String())
}

func getTestPost(user *entity.User) *entity.Post {
	text := ""
	k := testutils.RandInt(10, 200)

	for i := 0; i < k; i++ {
		text += random.String(uint8(testutils.RandInt(i, 245)))
	}

	return &entity.Post{
		UserID:    user.ID,
		Name:      random.String(222),
		Image:     random.String(199),
		Text:      text,
		DTCreated: time.Time{},
		DTUpdated: time.Time{},
		Author: &entity.Author{
			ID:   user.ID,
			Name: user.FirstName + " " + user.LastName,
		},
	}
}
