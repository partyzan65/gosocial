package mysql

import (
	"context"
	"database/sql"
	"fmt"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"gitlab.com/partyzan65/gosocial/pkg/datetime"
	"gitlab.com/partyzan65/gosocial/pkg/entity"
	"gitlab.com/partyzan65/gosocial/pkg/executor"
	"gitlab.com/partyzan65/gosocial/pkg/post"
)

const (
	defaultLimit = 200
)

type Repository struct {
	db *sqlx.DB
}

func New(db *sql.DB) *Repository {
	return &Repository{
		db: sqlx.NewDb(db, "mysql"),
	}
}

func (repo *Repository) Search(ctx context.Context, filter *post.Filter) (posts []*entity.Post, count int64, err error) {
	limit, offset, d := defaultLimit, 0, 1

	var (
		order = "dt_created desc"
		where = "where 1"
		args  []interface{}
	)

	if filter != nil {
		if filter.Limit > 0 {
			limit = filter.Limit
		}
		if filter.Offset > 0 {
			offset = filter.Offset
		}

		if n := len(filter.IDs); n > 0 {
			ids := make([]string, n)
			for i, id := range filter.IDs {
				ids[i] = strconv.FormatInt(id, 10)
			}

			where += ` and id in (` + strings.Join(ids, ",") + ")"
		}

		if filter.UserID > 0 {
			where += ` and user_id = ?`
			args = append(args, filter.UserID)
		}

		if filter.WithCount {
			d = 2
		}
	}

	_, ex := executor.FromContext(ctx, repo.db)

	var err1, err2 error

	wg := &sync.WaitGroup{}
	wg.Add(d)

	go func() {
		defer wg.Done()
		query := fmt.Sprintf(selectQuery, where, order, limit, offset)
		err1 = ex.Select(&posts, query, args...)
	}()

	if d > 1 {
		go func() {
			defer wg.Done()
			query := fmt.Sprintf(countQuery, where)
			err2 = ex.Get(&count, query, args...)
		}()
	}

	wg.Wait()

	if err1 != nil && err1 != sql.ErrNoRows {
		return nil, count, errors.Wrap(err1, "search for posts failed")
	}
	if err2 != nil {
		return nil, count, errors.Wrap(err2, "getting count of posts failed")
	}

	return
}

func (repo *Repository) Create(ctx context.Context, post entity.Post) (result *entity.Post, err error) {
	var tx *sqlx.Tx

	tx, err = repo.db.Beginx()
	if err != nil {
		return nil, errors.Wrap(err, "crating transaction failed")
	}
	defer func() {
		if err == nil {
			err = tx.Commit()
			return
		}

		err = tx.Rollback()
	}()

	stmt, err := tx.PrepareNamed(insertQuery)
	if err != nil {
		return nil, errors.Wrap(err, "preparing named statement failed")
	}

	post.DTCreated = datetime.Time(time.Now())
	post.DTUpdated = post.DTCreated

	res, err := stmt.Exec(&post)
	if err != nil {
		return nil, errors.Wrap(err, "executing query failed")
	}

	post.ID, err = res.LastInsertId()
	if err != nil {
		return nil, errors.Wrap(err, "getting last insert id failed")
	}

	return &post, err
}

func (repo *Repository) Update(ctx context.Context, post entity.Post) (result *entity.Post, err error) {
	var tx *sqlx.Tx

	tx, err = repo.db.Beginx()
	if err != nil {
		return nil, errors.Wrap(err, "crating transaction failed")
	}
	defer func() {
		if err == nil {
			err = tx.Commit()
			return
		}

		err = tx.Rollback()
	}()

	fields := []string{
		"user_id = :user_id", "name = :name", "image = :image",
		"text = :text", "dt_created = :dt_created", "dt_updated = :dt_updated",
	}

	query := fmt.Sprintf(updateQuery, strings.Join(fields, ", "))

	stmt, err := tx.PrepareNamed(query)
	if err != nil {
		return nil, errors.Wrap(err, "preparing named statement failed")
	}

	post.DTUpdated = datetime.Time(time.Now())

	_, err = stmt.Exec(&post)
	if err != nil {
		return nil, errors.Wrap(err, "executing query failed")
	}

	return &post, err
}

func (repo *Repository) Delete(ctx context.Context, post entity.Post) (err error) {
	var tx *sqlx.Tx

	tx, err = repo.db.Beginx()
	if err != nil {
		return errors.Wrap(err, "crating transaction failed")
	}
	defer func() {
		if err == nil {
			err = tx.Commit()
			return
		}

		err = tx.Rollback()
	}()

	_, err = tx.Exec(deleteQuery, post.ID)
	if err != nil {
		return errors.Wrap(err, "deleting post failed")
	}

	return
}
