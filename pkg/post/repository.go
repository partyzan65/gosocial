package post

import (
	"context"

	"gitlab.com/partyzan65/gosocial/pkg/entity"
)

type Filter struct {
	IDs           []int64
	UserID        int64
	Limit, Offset int
	WithCount     bool
}

type Repository interface {
	Search(ctx context.Context, filter *Filter) ([]*entity.Post, int64, error)
	Create(ctx context.Context, post entity.Post) (*entity.Post, error)
	Update(ctx context.Context, post entity.Post) (*entity.Post, error)
	Delete(ctx context.Context, post entity.Post) error
}
