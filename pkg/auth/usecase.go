package auth

import (
	"context"
	"crypto/sha256"
	"encoding/hex"
	"strconv"
	"strings"
	"time"

	"github.com/asticode/go-astitools/string"
	"github.com/pkg/errors"
	"gitlab.com/partyzan65/gosocial/pkg/entity"
	"gitlab.com/partyzan65/gosocial/pkg/user"
)

type UseCase struct {
	Repository

	users *user.UseCase
}

func (uc *UseCase) SearchToken(ctx context.Context, token string) (*entity.Token, error) {
	authToken, err := uc.Repository.Search(ctx, token)
	if err != nil {
		return nil, errors.Wrap(err, "search token failed")
	}

	authToken.User, err = uc.users.SearchByID(ctx, authToken.UserID)
	if err != nil {
		return nil, errors.Wrap(err, "search user failed")
	}

	return authToken, nil
}

func (uc *UseCase) CreateAuthToken(ctx context.Context, user *entity.User) (*entity.Token, error) {
	uniq := []string{
		strconv.FormatInt(user.ID, 10),
		strconv.FormatInt(time.Now().Unix(), 10),
		user.Login, astistring.RandomString(32),
	}

	t := sha256.Sum256([]byte(strings.Join(uniq, "_")))
	token := &entity.Token{
		User:      user,
		UserID:    user.ID,
		Type:      entity.AuthToken,
		Token:     hex.EncodeToString(t[:]),
		DTExpired: time.Now().Add(24 * time.Hour),
	}

	token, err := uc.Repository.Create(ctx, *token)
	if err != nil {
		return nil, errors.Wrap(err, "creating token failed")
	}

	return token, nil
}

func NewUseCase(users *user.UseCase, repo Repository) *UseCase {
	return &UseCase{
		users:      users,
		Repository: repo,
	}
}
