package auth

import (
	"context"

	"gitlab.com/partyzan65/gosocial/pkg/entity"
)

type Repository interface {
	Search(ctx context.Context, token string) (*entity.Token, error)
	Create(ctx context.Context, token entity.Token) (*entity.Token, error)
}
