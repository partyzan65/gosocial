package postgres

import (
	"context"
	"database/sql"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"gitlab.com/partyzan65/gosocial/pkg/entity"
)

const (
	searchQuery = `
select 
	user_id, token, type, dt_expired, dt_created 
from 
	public.auth_token 
where 
	token = $1
`
	insertQuery = `
insert into public.auth_token (user_id, token, type, dt_expired)
values (:user_id, :token, :type, :dt_expired)
`
)

type Repository struct {
	db *sqlx.DB
}

func (repo *Repository) Search(ctx context.Context, token string) (*entity.Token, error) {
	authToken := entity.Token{}

	err := repo.db.Get(&authToken, searchQuery, token)
	if err == sql.ErrNoRows {
		return nil, entity.ErrTokenNotFound
	}
	if err != nil {
		return nil, errors.Wrap(err, "search for token failed")
	}

	//user, err := repo.users.SearchByID(ctx, authToken.UserID)
	//if err != nil {
	//	return nil, errors.Wrap(err, "search user by token failed")
	//}
	//
	//authToken.User = user

	return &authToken, nil
}

func (repo *Repository) Create(ctx context.Context, token entity.Token) (t *entity.Token, err error) {
	var tx *sqlx.Tx

	tx, err = repo.db.Beginx()
	if err != nil {
		return nil, errors.Wrap(err, "crating transaction failed")
	}
	defer func() {
		if err == nil {
			err = tx.Commit()
			return
		}

		err = tx.Rollback()
	}()

	stmt, err := tx.PrepareNamed(insertQuery)
	if err != nil {
		return nil, errors.Wrap(err, "preparing named statement failed")
	}

	_, err = stmt.Exec(&token)
	if err != nil {
		return nil, errors.Wrap(err, "executing query failed")
	}

	return &token, nil
}

func New(db *sql.DB) *Repository {
	return &Repository{
		db: sqlx.NewDb(db, "postgres"),
	}
}
