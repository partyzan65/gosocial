package auth_test

import (
	"context"
	"testing"
	"time"

	"github.com/asticode/go-astitools/string"
	"github.com/partyzanex/testutils"
	"gitlab.com/partyzan65/gosocial/pkg/auth"
	"gitlab.com/partyzan65/gosocial/pkg/datetime"
	"gitlab.com/partyzan65/gosocial/pkg/entity"
	"gitlab.com/partyzan65/gosocial/pkg/user"

	tokens "gitlab.com/partyzan65/gosocial/pkg/auth/repository/mysql"
	regions "gitlab.com/partyzan65/gosocial/pkg/region/repository"
	users "gitlab.com/partyzan65/gosocial/pkg/user/repository/mysql"
)

func TestRepository_CreateAuthToken(t *testing.T) {
	db := testutils.NewSqlDB(t, "mysql", "MYSQL_TEST")

	userRepo := users.New(db)
	regionRepo := regions.New(db)
	userCase := user.NewUseCase(userRepo, regionRepo)
	useCase := auth.NewUseCase(userCase, tokens.New(db))

	ctx := context.Background()

	u, err := userRepo.Create(ctx, *getTestUser())
	testutils.FatalErr(t, "users.Create", err)

	exp, err := useCase.CreateAuthToken(ctx, u)
	testutils.FatalErr(t, "repo.CreateAuthToken", err)

	got, err := useCase.SearchToken(ctx, exp.Token)
	testutils.FatalErr(t, "repo.Search", err)

	testutils.AssertEqual(t, "User.ID", exp.User.ID, got.User.ID)
	testutils.AssertEqual(t, "User.FirstName", exp.User.FirstName, got.User.FirstName)
	testutils.AssertEqual(t, "User.LastName", exp.User.LastName, got.User.LastName)
	testutils.AssertEqual(t, "User.Login", exp.User.Login, got.User.Login)
	testutils.AssertEqual(t, "Token", exp.Token, got.Token)
	testutils.AssertEqual(t, "UserID", exp.UserID, got.UserID)
	testutils.AssertEqual(t, "Type", exp.Type, got.Type)
	testutils.AssertEqual(t, "DTExpired", exp.DTExpired.Unix(), got.DTExpired.Unix())
}

func getTestUser() *entity.User {
	return &entity.User{
		Login:       astistring.RandomString(24),
		Password:    astistring.RandomString(64),
		Status:      entity.UserNew,
		RegionID:    101,
		FirstName:   astistring.RandomString(18),
		LastName:    astistring.RandomString(18),
		Gender:      entity.Female,
		BDate:       datetime.Date(time.Now()),
		City:        astistring.RandomString(30),
		Description: astistring.RandomString(3992),
	}
}
