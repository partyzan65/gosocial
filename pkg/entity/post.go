package entity

import (
	"time"
)

type Post struct {
	ID        int64     `json:"id" db:"id"`
	UserID    int64     `json:"user_id" db:"user_id"`
	Name      string    `json:"name" db:"name"`
	Image     string    `json:"image" db:"image"`
	Text      string    `json:"text" db:"text"`
	DTCreated time.Time `json:"dt_created" db:"dt_created"`
	DTUpdated time.Time `json:"dt_updated" db:"dt_updated"`

	Author *Author `json:"author" db:"-"`
}

type Author struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}
