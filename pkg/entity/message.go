package entity

import (
	"gitlab.com/partyzan65/gosocial/pkg/datetime"
)

type Message struct {
	UserID    uint64 `json:"user_id" db:"user_id"`
	FromID    uint64 `json:"from" db:"from_id"`
	ToID      uint64 `json:"to_id" db:"to_id"`
	RegionID  int    `json:"region_id" db:"region_id"`
	DTCreated uint64 `json:"dt_created" db:"dt_created"`
	HasRead   bool   `json:"has_read" db:"has_read"`
	Text      string `json:"message" db:"message"`

	DT   datetime.Date `json:"dt"`
	From *User         `json:"from"`
	To   *User         `json:"to"`
}

func (message Message) Timestamp() uint64 {
	return message.DTCreated / 1000000
}

type Messages []*Message

type DialogEntry struct {
	FromID    uint64 `json:"from_id" db:"from_id"`
	DTCreated uint64 `json:"dt_created" db:"dt_created"`
	//HasRead   bool   `json:"has_read" db:"has_read"`
	//Text      bool   `json:"text" db:"message"`
}

type Dialogs []*DialogEntry
