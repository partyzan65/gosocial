package entity

type Feed struct {
	UserID int64
	Posts  []*Post
}
