package entity

import (
	"crypto/sha256"
	"encoding/hex"
	"strconv"
	"strings"
	"time"

	"gitlab.com/partyzan65/gosocial/pkg/datetime"
)

const (
	UserNew     UserStatus = "new"
	UserActive  UserStatus = "active"
	UserBlocked UserStatus = "blocked"

	Male   Gender = "male"
	Female Gender = "female"

	UserContextKey = "user"
)

type UserStatus string

func (status UserStatus) IsValid() bool {
	switch status {
	case UserNew, UserActive, UserBlocked:
		return true
	}

	return false
}

type Gender string

func (g Gender) IsValid() bool {
	return g == Male || g == Female
}

type User struct {
	ID int64 `db:"id" json:"id"`

	Login    string     `db:"login" json:"login"`
	Password string     `db:"password" json:"password"`
	Status   UserStatus `db:"status" json:"status"`

	FirstName   string        `db:"first_name" json:"first_name"`
	LastName    string        `db:"last_name" json:"last_name"`
	BDate       datetime.Date `db:"bdate" json:"bdate"`
	Gender      Gender        `db:"gender" json:"gender"`
	Description string        `db:"description" json:"description"`
	RegionID    int           `db:"region_id" json:"region_id"`
	City        string        `db:"city" json:"city"`

	DTCreated    time.Time `db:"dt_created" json:"dt_created"`
	DTUpdated    time.Time `db:"dt_updated" json:"dt_updated"`
	DTLastLogged time.Time `db:"dt_last_logged" json:"dt_last_logged"`

	PasswordIsEncoded bool    `db:"password_is_encoded" json:"-"`
	Current           bool    `db:"-" json:"-"`
	Region            *Region `json:"region" db:"-"`
}

func (user User) Age() string {
	if time.Time(user.BDate).IsZero() {
		return ""
	}

	age := time.Now().Sub(time.Time(user.BDate))
	return strconv.Itoa(int(age.Hours() / (24 * 365)))
}

func (user User) Created() string {
	return user.DTCreated.Format(datetime.TimeFormat)
}

func (user User) LastLogged() string {
	if user.DTLastLogged.IsZero() {
		return ""
	}

	return user.DTLastLogged.Format(datetime.TimeFormat)
}

func (user User) Avatar() string {
	id := strconv.FormatInt(user.ID, 10)
	hash := sha256.Sum256([]byte(user.DTCreated.String() + "--" + id))
	return id + "/" + hex.EncodeToString(hash[:]) + ".jpg"
}

func (user User) HtmlDesc() string {
	return strings.Replace(user.Description, "\n", `<br />`, -1)
}
