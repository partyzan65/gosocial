package entity

import (
	"github.com/pkg/errors"
	"time"
)

var (
	ErrTokenNotFound = errors.New("token not found")
)

type Token struct {
	UserID    int64     `db:"user_id"`
	Token     string    `db:"token"`
	Type      TokenType `db:"type"`
	DTExpired time.Time `db:"dt_expired"`
	DTCreated time.Time `db:"dt_created"`

	User *User `db:"-"`
}

func (t Token) IsExpired() bool {
	return time.Now().After(t.DTExpired)
}

type TokenType string

func (t TokenType) IsValid() bool {
	return t == AuthToken
}

const (
	AuthToken TokenType = "auth"
)
