package subscription

type Repository interface {
	Subscribe(userID, subscriberID int64) error
	Unsubscribe(userID, subscriberID int64) error
	Subscribers(userID int64, limit, offset int) ([]int64, error)
	SubscribersCount(userID int64) (int64, error)
	Subscriptions(userID int64, limit, offset int) ([]int64, error)
	SubscriptionsCount(userID int64) (int64, error)
}
