package mysql

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
)

type Repository struct {
	db *sqlx.DB
}

func New(db *sql.DB) *Repository {
	return &Repository{
		db: sqlx.NewDb(db, "mysql"),
	}
}

func (repo *Repository) Subscribe(userID, subscriberID int64) (err error) {
	var tx *sqlx.Tx

	tx, err = repo.db.Beginx()
	if err != nil {
		return errors.Wrap(err, "crating transaction failed")
	}
	defer func() {
		if err == nil {
			err = tx.Commit()
			return
		}

		err = tx.Rollback()
	}()

	stmt, err := tx.PrepareNamed(subscribeQuery)
	if err != nil {
		return errors.Wrap(err, "preparing named statement failed")
	}

	_, err = stmt.Exec(&subscription{
		UserID:       userID,
		SubscriberID: subscriberID,
	})
	if err != nil {
		return errors.Wrap(err, "executing query failed")
	}

	return
}

func (repo *Repository) Unsubscribe(userID, subscriberID int64) (err error) {
	var tx *sqlx.Tx

	tx, err = repo.db.Beginx()
	if err != nil {
		return errors.Wrap(err, "crating transaction failed")
	}
	defer func() {
		if err == nil {
			err = tx.Commit()
			return
		}

		err = tx.Rollback()
	}()

	_, err = tx.Exec(unsubscribeQuery, userID, subscriberID)
	if err != nil {
		return errors.Wrap(err, "deleting subscription failed failed")
	}

	return
}

func (repo *Repository) Subscribers(userID int64, limit, offset int) (subscribers []int64, err error) {
	query := fmt.Sprintf(selectSubscribers, limit, offset)

	err = repo.db.Select(&subscribers, query, userID)
	if err != nil && err != sql.ErrNoRows {
		return nil, errors.Wrap(err, "search for subscribers failed")
	}

	return
}

func (repo *Repository) SubscribersCount(userID int64) (count int64, err error) {
	err = repo.db.Get(&count, countSubscribers, userID)
	if err != nil {
		return count, errors.Wrap(err, "get count of subscribers failed")
	}

	return
}

func (repo *Repository) Subscriptions(userID int64, limit, offset int) (subscriptions []int64, err error) {
	query := fmt.Sprintf(selectSubscriptions, limit, offset)

	err = repo.db.Select(&subscriptions, query, userID)
	if err != nil && err != sql.ErrNoRows {
		return nil, errors.Wrap(err, "search for subscriptions failed")
	}

	return
}

func (repo *Repository) SubscriptionsCount(userID int64) (count int64, err error) {
	err = repo.db.Get(&count, countSubscriptions, userID)
	if err != nil {
		return count, errors.Wrap(err, "get count of subscriptions failed")
	}

	return
}
