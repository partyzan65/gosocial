package mysql_test

import (
	"context"
	"github.com/labstack/gommon/random"
	"github.com/partyzanex/testutils"
	"gitlab.com/partyzan65/gosocial/pkg/datetime"
	"gitlab.com/partyzan65/gosocial/pkg/entity"
	"gitlab.com/partyzan65/gosocial/pkg/subscription/repository/mysql"
	users "gitlab.com/partyzan65/gosocial/pkg/user/repository/mysql"
	"testing"
	"time"
)

func TestNew(t *testing.T) {
	db := testutils.NewSqlDB(t, "mysql", "MYSQL_TEST")

	ctx := context.Background()

	repo := mysql.New(db)
	usersRepo := users.New(db)

	var err error

	user := getTestUser()

	user, err = usersRepo.Create(ctx, *user)
	testutils.FatalErr(t, "usersRepo.Create(ctx, *user)", err)

	defer func() {
		err := usersRepo.Delete(ctx, *user)
		testutils.Err(t, "users.Delete(ctx, *user)", err)
	}()

	s := 10
	subscribers := make([]*entity.User, s)

	for i := 0; i < s; i++ {
		var err error

		subscriber := getTestUser()

		subscriber, err = usersRepo.Create(ctx, *subscriber)
		testutils.Err(t, "usersRepo.Create(ctx, *subscriber)", err)
		if err != nil {
			return
		}

		subscribers[i] = subscriber

		err = repo.Subscribe(user.ID, subscriber.ID)
		testutils.Err(t, "repo.Subscribe(user.ID, subscriber.ID)", err)
	}

	defer func() {
		for i := 0; i < s; i++ {
			err := repo.Unsubscribe(user.ID, subscribers[i].ID)
			testutils.Err(t, "repo.Unsubscribe(user.ID, subscribers[i].ID)", err)

			err = usersRepo.Delete(ctx, *subscribers[i])
			testutils.Err(t, "usersRepo.Delete(ctx, *subscribers[i])", err)
		}
	}()

	count, err := repo.SubscribersCount(user.ID)
	testutils.Err(t, "repo.SubscribersCount(user.ID)", err)

	testutils.AssertEqual(t, "count", int64(s), count)

	subIDs, err := repo.Subscribers(user.ID, s*10, 0)

	testutils.AssertEqual(t, "subIDs", s, len(subIDs))

	count, err = repo.SubscriptionsCount(subscribers[0].ID)

	testutils.AssertEqual(t, "count", int64(1), count)

	supIDs, err := repo.Subscriptions(subscribers[0].ID, 10, 0)

	testutils.AssertEqual(t, "supIDs", 1, len(supIDs))
}

func getTestUser() *entity.User {
	return &entity.User{
		ID:                testutils.RandInt64(9999999, 99999999),
		Login:             random.String(50),
		Password:          "",
		Status:            entity.UserBlocked,
		FirstName:         random.String(20),
		LastName:          random.String(20),
		BDate:             datetime.Date(time.Now().AddDate(-20, 0, 0)),
		Gender:            entity.Female,
		Description:       random.String(200),
		RegionID:          101,
		City:              random.String(20),
		PasswordIsEncoded: true,
		Current:           false,
	}
}
