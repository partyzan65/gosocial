package mysql

const (
	subscribeQuery      = `insert into subscription (user_id, subscriber_id) values (:user_id, :subscriber_id)`
	unsubscribeQuery    = `delete from subscription where user_id = ? and subscriber_id = ?`
	selectSubscribers   = `select subscriber_id from subscription where user_id = ? limit %d offset %d`
	countSubscribers    = `select count(*) from subscription where user_id = ?`
	selectSubscriptions = `select user_id from subscription where subscriber_id = ? limit %d offset %d`
	countSubscriptions  = `select count(*) from subscription where subscriber_id = ?`
)

type subscription struct {
	UserID       int64 `db:"user_id"`
	SubscriberID int64 `db:"subscriber_id"`
}
