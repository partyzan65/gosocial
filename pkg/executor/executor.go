package executor

import "context"

type Executor interface {
	Select(dst interface{}, query string, args ...interface{}) error
	Get(dst interface{}, query string, args ...interface{}) error
}

type executorKey struct {
	Name string
}

var Key = executorKey{Name: "Executor"}

func FromContext(ctx context.Context, ex Executor) (context.Context, Executor) {
	if ctx == nil {
		return context.Background(), ex
	}

	e, ok := ctx.Value(Key).(Executor)
	if !ok || e == nil {
		return ctx, ex
	}

	return ctx, e
}

func WithExecutor(ctx context.Context, ex Executor) context.Context {
	return context.WithValue(ctx, Key, ex)
}
