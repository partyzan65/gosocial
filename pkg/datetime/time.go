package datetime

import (
	"database/sql/driver"
	"encoding/json"
	"time"

	"github.com/pkg/errors"
)

type Time time.Time

func (d Time) MarshalJSON() ([]byte, error) {
	b, err := json.Marshal(time.Time(d).Format(TimeFormat))
	if err != nil {
		return nil, errors.Wrap(err, "marshaling date failed")
	}

	return b, nil
}

func (d *Time) UnmarshalJSON(b []byte) error {
	var s string

	err := json.Unmarshal(b, &s)
	if err != nil {
		return errors.Wrap(err, "unmarshal date failed")
	}

	parsed, err := time.ParseInLocation(TimeFormat, s, time.Local)
	if err != nil {
		return errors.Wrapf(err, "parsing date %s failed", s)
	}

	*d = Time(parsed)

	return nil
}

func (d Time) Value() (driver.Value, error) {
	return time.Time(d).Format(SqlFormat), nil
}

func (d *Time) Scan(src interface{}) error {
	switch src.(type) {
	case string:
		str := src.(string)

		dt, err := time.ParseInLocation(SqlFormat, str, time.Local)
		if err != nil {
			return errors.Wrapf(err, "cannot parse date %s using layout %s", str, SqlFormat)
		}

		*d = Time(dt)
	case time.Time:
		*d = Time(src.(time.Time))
		return nil
	case []byte:
		str := string(src.([]byte))

		dt, err := time.ParseInLocation(SqlFormat, str, time.Local)
		if err != nil {
			return errors.Wrapf(err, "parsing date %s failed with layout %s", str, SqlFormat)
		}

		*d = Time(dt)
	default:
		return errors.Errorf("unsupported type of value %#v", src)
	}

	return nil
}

func (d Time) String() string {
	return time.Time(d).Format(DateFormat)
}
