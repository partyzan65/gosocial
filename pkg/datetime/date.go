package datetime

import (
	"database/sql/driver"
	"encoding/json"
	"time"

	"github.com/pkg/errors"
)

type Date time.Time

const (
	DateFormat = "01/02/2006"
	TimeFormat = "01/02/2006 15:04:05"
	SqlFormat  = "2006-01-02"
)

func (d Date) MarshalJSON() ([]byte, error) {
	b, err := json.Marshal(time.Time(d).Format(DateFormat))
	if err != nil {
		return nil, errors.Wrap(err, "marshaling date failed")
	}

	return b, nil
}

func (d *Date) UnmarshalJSON(b []byte) error {
	var s string

	err := json.Unmarshal(b, &s)
	if err != nil {
		return errors.Wrap(err, "unmarshal date failed")
	}

	parsed, err := time.ParseInLocation(DateFormat, s, time.Local)
	if err != nil {
		return errors.Wrapf(err, "parsing date %s failed", s)
	}

	*d = Date(parsed)

	return nil
}

func (d Date) Value() (driver.Value, error) {
	return time.Time(d).Format(SqlFormat), nil
}

func (d *Date) Scan(src interface{}) error {
	switch src.(type) {
	case string:
		str := src.(string)

		dt, err := time.ParseInLocation(SqlFormat, str, time.Local)
		if err != nil {
			return errors.Wrapf(err, "cannot parse date %s using layout %s", str, SqlFormat)
		}

		*d = Date(dt)
	case time.Time:
		*d = Date(src.(time.Time))
		return nil
	case []byte:
		str := string(src.([]byte))

		dt, err := time.ParseInLocation(SqlFormat, str, time.Local)
		if err != nil {
			return errors.Wrapf(err, "parsing date %s failed with layout %s", str, SqlFormat)
		}

		*d = Date(dt)
	default:
		return errors.Errorf("unsupported type of value %#v", src)
	}

	return nil
}

func (d Date) String() string {
	return time.Time(d).Format(DateFormat)
}

func (d Date) TimeString() string {
	return time.Time(d).Format(TimeFormat)
}
