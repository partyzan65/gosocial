package message

import (
	"context"
	"gitlab.com/partyzan65/gosocial/pkg/entity"
)

type Filter struct {
	UserID, FromID uint64
	Limit, Offset  int
	Order          string
}

type Repository interface {
	Create(ctx context.Context, message *entity.Message) error
	Messages(ctx context.Context, filter Filter) (entity.Messages, error)
	Dialogs(ctx context.Context, filter Filter) (entity.Dialogs, error)
	SetRead(ctx context.Context, userID, fromID uint64) error
}
