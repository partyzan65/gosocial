package repository

const (
	insertQuery = `
insert into messages 
	(user_id, from_id, to_id, region_id, dt_created, has_read, message)
values
	(:user_id, :from_id, :to_id, :region_id, :dt_created, :has_read, :message)
`
	setReadQuery = `
update messages set
	has_read = true
where user_id = ? and to_id = ? and has_read = ?
`
	messagesQuery = `
select 
	user_id, to_id, from_id, region_id, dt_created, has_read, message
from 
	messages
where 
	user_id = ? and (from_id = ? or to_id = ?)
order by %s 
%s
`
	dialogsQuery = `
select from_id, max(dt_created) as dt_created
from messages
where user_id = ? and from_id <> ?
group by from_id
%s
order by dt_created desc
`
)
