package repository

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"gitlab.com/partyzan65/gosocial/pkg/datetime"
	"gitlab.com/partyzan65/gosocial/pkg/entity"
	"gitlab.com/partyzan65/gosocial/pkg/message"
	"time"
)

type repository struct {
	ex *sqlx.DB
}

func New(db *sql.DB) *repository {
	return &repository{ex: sqlx.NewDb(db, "mysql")}
}

func (repo *repository) Create(ctx context.Context, message *entity.Message) (err error) {
	tx, err := repo.ex.Beginx()
	if err != nil {
		return errors.Wrap(err, "crating transaction failed")
	}
	defer func() {
		if err == nil {
			err = tx.Commit()
			return
		}

		err = tx.Rollback()
	}()

	stmt, err := tx.PrepareNamed(insertQuery)
	if err != nil {
		return errors.Wrap(err, "preparing named statement failed")
	}

	message.DTCreated = uint64(time.Now().UnixNano())

	from := *message
	to := *message

	from.UserID = message.FromID
	from.ToID = message.ToID
	from.FromID = message.FromID
	from.RegionID = message.From.RegionID

	to.UserID = message.ToID
	to.ToID = message.ToID
	to.FromID = message.FromID
	to.RegionID = message.To.RegionID

	_, err = stmt.Exec(&from)
	if err != nil {
		return errors.Wrap(err, "executing query failed")
	}

	_, err = stmt.Exec(&to)
	if err != nil {
		return errors.Wrap(err, "executing query failed")
	}

	return nil
}

func (repo *repository) Messages(ctx context.Context, filter message.Filter) (entity.Messages, error) {
	var messages entity.Messages

	limitTpl := ""
	orderTpl := "dt_created"

	if filter.Limit > 0 {
		limitTpl = fmt.Sprintf("limit %d offset %d", filter.Limit, filter.Offset)
	}

	if filter.Order != "" {
		orderTpl = filter.Order
	}

	query := fmt.Sprintf(messagesQuery, orderTpl, limitTpl)

	err := repo.ex.Select(&messages, query, filter.UserID, filter.FromID, filter.FromID)
	if err != nil {
		return nil, errors.Wrap(err, "select messages failed")
	}

	for _, msg := range messages {
		nano := int64(msg.DTCreated / 1000000000)
		msg.DT = datetime.Date(time.Unix(nano, int64(msg.DTCreated-uint64(nano*1000000000))))
	}

	return messages, nil
}

func (repo *repository) Dialogs(ctx context.Context, filter message.Filter) (dialogs entity.Dialogs, err error) {
	limitTpl := ""

	if filter.Limit > 0 {
		limitTpl = fmt.Sprintf("limit %d offset %d", filter.Limit, filter.Offset)
	}

	query := fmt.Sprintf(dialogsQuery, limitTpl)

	err = repo.ex.Select(&dialogs, query, filter.UserID, filter.UserID)
	if err != nil {
		return nil, errors.Wrap(err, "select messages failed")
	}

	return
}

func (repo *repository) SetRead(ctx context.Context, userID, fromID uint64) error {
	_, err := repo.ex.Exec(setReadQuery, userID, fromID, false)
	if err != nil {
		return errors.Wrap(err, "set read from failed")
	}

	return nil
}
