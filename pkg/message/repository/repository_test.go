package repository_test

import (
	"context"
	astistring "github.com/asticode/go-astitools/string"
	"github.com/partyzanex/testutils"
	"gitlab.com/partyzan65/gosocial/pkg/datetime"
	"gitlab.com/partyzan65/gosocial/pkg/entity"
	"gitlab.com/partyzan65/gosocial/pkg/message"
	"gitlab.com/partyzan65/gosocial/pkg/message/repository"
	"testing"
)

func TestNew(t *testing.T) {
	db := testutils.NewSqlDB(t, "mysql", "MYSQL_TEST")

	repo := repository.New(db)

	from := getTestUser()
	to := getTestUser()

	msg := getTestMessage(from, to)

	ctx := context.Background()

	err := repo.Create(ctx, msg)
	testutils.Err(t, "repo.Create(ctx, message)", err)
	defer func() {
		_, err := db.Exec(`delete from messages where user_id = ?`, from.ID)
		testutils.Err(t, "db.Exec(`delete from messages where user_id = ?`, from.ID)", err)
	}()

	//err = repo.SetRead(ctx, uint64(from.ID), uint64(to.ID))
	//testutils.Err(t, "repo.SetRead(ctx, uint64(from.ID), uint64(to.ID))", err)

	reply := getTestMessage(to, from)

	err = repo.Create(ctx, reply)
	testutils.Err(t, "repo.Create(ctx, reply)", err)
	defer func() {
		_, err := db.Exec(`delete from messages where user_id = ?`, to.ID)
		testutils.Err(t, "db.Exec(`delete from messages where user_id = ?`, to.ID)", err)
	}()

	//err = repo.SetRead(ctx, uint64(to.ID), uint64(from.ID))
	//testutils.Err(t, "repo.SetRead(ctx, uint64(from.ID), uint64(to.ID))", err)

	messages, err := repo.Messages(ctx, message.Filter{
		UserID: uint64(from.ID),
		FromID: uint64(from.ID),
	})
	testutils.Err(t, "repo.Messages", err)
	testutils.AssertEqual(t, "count of messages", 2, len(messages))

	//for _, m := range messages {
	//	testutils.AssertEqual(t, "has read", true, m.HasRead)
	//}

	messages, err = repo.Messages(ctx, message.Filter{
		UserID: uint64(to.ID),
		FromID: uint64(to.ID),
	})
	testutils.Err(t, "repo.Messages", err)
	testutils.AssertEqual(t, "count of messages", 2, len(messages))

	dialogs, err := repo.Dialogs(ctx, message.Filter{
		UserID: uint64(from.ID),
	})
	testutils.Err(t, "repo.Dialogs", err)
	testutils.AssertEqual(t, "count of dialogs", 1, len(dialogs))

	dialogs, err = repo.Dialogs(ctx, message.Filter{
		UserID: uint64(to.ID),
	})
	testutils.Err(t, "repo.Dialogs", err)
	testutils.AssertEqual(t, "count of dialogs", 1, len(dialogs))
}

func getTestUser() *entity.User {
	return &entity.User{
		ID:       testutils.RandInt64(1, 999),
		RegionID: testutils.RandInt(1, 99),
	}
}

func getTestMessage(from, to *entity.User) *entity.Message {
	return &entity.Message{
		UserID:  uint64(to.ID),
		FromID:  uint64(from.ID),
		ToID:    uint64(to.ID),
		HasRead: false,
		Text:    astistring.RandomString(testutils.RandInt(40, 10000)),
		DT:      datetime.Date{},
		From:    from,
		To:      to,
	}
}
