package feed

import "gitlab.com/partyzan65/gosocial/pkg/entity"

type Repository interface {
	Get(userID int64) (*entity.Feed, error)
	Set(feed entity.Feed) error
	Remove(userID int64) error
}
