package tarantool

import (
	"bytes"
	"encoding/gob"
	"github.com/pkg/errors"
	"github.com/tarantool/go-tarantool"
	"gitlab.com/partyzan65/gosocial/pkg/entity"
	"sync"
)

type gobState struct {
	*bytes.Buffer

	encoder *gob.Encoder
	decoder *gob.Decoder
}

type Repository struct {
	conn *tarantool.Connection
	pool *sync.Pool
}

func New(conn *tarantool.Connection) *Repository {
	return &Repository{
		conn: conn,
	}
}

func (repo *Repository) Get(userID int64) (*entity.Feed, error) {
	key := []interface{}{userID}

	resp, err := repo.conn.Select("feed", "primary", 0, 1, tarantool.IterEq, key)
	if err != nil {
		return nil, errors.Wrap(err, "get feed by user id failed")
	}

	if n := len(resp.Data); n > 0 {
		feed := &entity.Feed{
			UserID: userID,
			Posts:  make([]*entity.Post, n-1),
		}
		buf := &bytes.Buffer{}
		state := &gobState{
			Buffer:  buf,
			decoder: gob.NewDecoder(buf),
		}
		pool := &sync.Pool{
			New: func() interface{} {
				return state
			},
		}

		for i, p := range resp.Data[1:] {
			state := pool.Get().(*gobState)

			_, err := state.WriteString(p.(string))
			if err != nil {
				return nil, errors.Wrap(err, "write post to buffer failed")
			}

			post := &entity.Post{}

			err = state.decoder.Decode(post)
			if err != nil {
				return nil, errors.Wrap(err, "decoding post failed")
			}

			feed.Posts[i] = post

			state.Reset()
		}

		return feed, nil
	}

	return nil, nil
}

func (repo *Repository) Set(feed entity.Feed) error {
	args := make([]interface{}, len(feed.Posts)+1)
	args[0] = feed.UserID

	buf := &bytes.Buffer{}
	state := &gobState{
		Buffer:  buf,
		encoder: gob.NewEncoder(buf),
	}
	pool := &sync.Pool{
		New: func() interface{} {
			return state
		},
	}

	for i, post := range feed.Posts {
		state := pool.Get().(*gobState)

		err := state.encoder.Encode(post)
		if err != nil {
			return errors.Wrap(err, "encoding post failed")
		}

		args[i+1] = state.String()

		state.Reset()
	}

	_, err := repo.conn.Replace("feed", args)
	if err != nil {
		return errors.Wrap(err, "insert feed failed")
	}

	return nil
}

func (repo *Repository) Remove(userID int64) error {
	_, err := repo.conn.Delete("feed", "primary", []interface{}{userID})
	if err != nil {
		return errors.Wrap(err, "delete feed failed")
	}

	return nil
}
