package redis

import (
	"bytes"
	"encoding/gob"
	"github.com/go-redis/redis/v7"
	"github.com/pkg/errors"
	"gitlab.com/partyzan65/gosocial/pkg/entity"
	"strconv"
	"sync"
)

type Repository struct {
	client *redis.Client
}

func New(client *redis.Client) *Repository {
	return &Repository{
		client: client,
	}
}

type gobState struct {
	*bytes.Buffer

	encoder *gob.Encoder
	decoder *gob.Decoder
}

func (repo *Repository) Get(userID int64) (*entity.Feed, error) {
	key := repo.key(userID)

	cmd := repo.client.ZRange(key, 0, -1)
	if err := cmd.Err(); err != nil {
		return nil, errors.Wrap(err, "get feed by user id failed")
	}

	posts := cmd.Val()

	if n := len(posts); n > 0 {
		feed := &entity.Feed{
			UserID: userID,
			Posts:  make([]*entity.Post, n),
		}
		buf := &bytes.Buffer{}
		state := &gobState{
			Buffer:  buf,
			decoder: gob.NewDecoder(buf),
		}
		pool := &sync.Pool{
			New: func() interface{} {
				return state
			},
		}

		for i, p := range posts {
			state := pool.Get().(*gobState)

			_, err := state.WriteString(p)
			if err != nil {
				return nil, errors.Wrap(err, "write post to buffer failed")
			}

			post := &entity.Post{}

			err = state.decoder.Decode(post)
			if err != nil {
				return nil, errors.Wrap(err, "decoding post failed")
			}

			feed.Posts[i] = post

			state.Reset()
		}

		return feed, nil
	}

	return nil, nil
}

func (repo *Repository) Set(userID int64, posts ...*entity.Post) error {
	buf := &bytes.Buffer{}
	state := &gobState{
		Buffer:  buf,
		encoder: gob.NewEncoder(buf),
	}
	pool := &sync.Pool{
		New: func() interface{} {
			return state
		},
	}

	args := make([]*redis.Z, len(posts))

	for i, post := range posts {
		state := pool.Get().(*gobState)

		err := state.encoder.Encode(post)
		if err != nil {
			return errors.Wrap(err, "encoding post failed")
		}

		args[i] = &redis.Z{
			Score:  float64(-1 * post.ID),
			Member: buf.String(),
		}

		state.Reset()
	}

	key := repo.key(userID)
	tmpKey := "tmp_" + key

	err := repo.client.Watch(func(tx *redis.Tx) error {
		cmd := tx.ZAdd(key, args...)
		if err := cmd.Err(); err != nil {
			return err
		}

		cmd = tx.ZRemRangeByRank(tmpKey, 1000, -1)
		if err := cmd.Err(); err != nil {
			return err
		}
		//
		//cmd = tx.Del(key)
		//if err := cmd.Err(); err != nil {
		//	return err
		//}
		//
		//imd := tx.Rename(tmpKey, key)
		//if err := imd.Err(); err != nil {
		//	return err
		//}

		return nil
	}, key, tmpKey)
	if err != nil {
		return errors.Wrap(err, "insert feed failed")
	}

	return nil
}

func (Repository) key(id int64) string {
	return "feed_" + strconv.FormatInt(id, 10)
}

func (repo *Repository) Remove(userID int64) error {
	key := repo.key(userID)
	cmd := repo.client.Del(key)

	if err := cmd.Err(); err != nil {
		return errors.Wrap(err, "delete feed failed")
	}

	return nil
}
