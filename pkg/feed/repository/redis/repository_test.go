package redis_test

import (
	"github.com/go-redis/redis/v7"
	"github.com/labstack/gommon/random"
	"github.com/partyzanex/testutils"
	"gitlab.com/partyzan65/gosocial/pkg/entity"
	feeds "gitlab.com/partyzan65/gosocial/pkg/feed/repository/redis"
	"os"
	"testing"
	"time"
)

func TestNew(t *testing.T) {
	dsn := os.Getenv("REDIS_TEST")

	if dsn == "" {
		t.Skip("empty REDIS_TEST")
	}

	client := redis.NewClient(&redis.Options{
		Addr: dsn,
		DB:   0,
	})

	repo := feeds.New(client)

	s := testutils.RandInt(10, 100)
	exp := entity.Feed{
		UserID: 99999,
		Posts:  make([]*entity.Post, s),
	}

	for i := 0; i < s; i++ {
		exp.Posts[i] = getTestPost()
	}

	err := repo.Set(exp)
	testutils.Err(t, "repo.Set(exp)", err)

	got, err := repo.Get(exp.UserID)
	testutils.Err(t, "repo.Get(exp.UserID)", err)

	testutils.AssertEqual(t, "UserID", exp.UserID, got.UserID)

	for i := range got.Posts {
		testutils.AssertEqual(t, "ID", exp.Posts[i].ID, got.Posts[i].ID)
		testutils.AssertEqual(t, "UserID", exp.Posts[i].UserID, got.Posts[i].UserID)
		testutils.AssertEqual(t, "Name", exp.Posts[i].Name, got.Posts[i].Name)
		testutils.AssertEqual(t, "Image", exp.Posts[i].Image, got.Posts[i].Image)
		testutils.AssertEqual(t, "Text", exp.Posts[i].Text, got.Posts[i].Text)
		testutils.AssertEqual(t, "DTCreated", exp.Posts[i].DTCreated.Unix(), got.Posts[i].DTCreated.Unix())
		testutils.AssertEqual(t, "DTUpdated", exp.Posts[i].DTUpdated.Unix(), got.Posts[i].DTUpdated.Unix())
	}

	err = repo.Remove(exp.UserID)
	testutils.Err(t, "repo.Remove(exp.UserID)", err)
}

func BenchmarkRepository_Get(b *testing.B) {
	dsn := os.Getenv("REDIS_TEST")

	if dsn == "" {
		b.Skip("empty REDIS_TEST")
	}

	client := redis.NewClient(&redis.Options{
		Addr: dsn,
		DB:   0,
	})

	repo := feeds.New(client)

	//s := testutils.RandInt(10, 100)
	s := 1000

	exp := entity.Feed{
		UserID: testutils.RandInt64(99999, 999999),
		Posts:  make([]*entity.Post, s),
	}

	for i := 0; i < s; i++ {
		exp.Posts[i] = getTestPost()
	}

	err := repo.Set(exp)
	testutils.Err(b, "repo.Set(exp)", err)

	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			_, err := repo.Get(exp.UserID)
			if err != nil {
				b.Fatal(err)
			}
		}
	})
	//
	//for i := 0; i < b.N; i++ {
	//	_, err := repo.Get(exp.UserID)
	//	if err != nil {
	//		b.Fatal(err)
	//	}
	//}

	err = repo.Remove(exp.UserID)
	testutils.Err(b, "repo.Remove(exp.UserID)", err)
}

func getTestPost() *entity.Post {
	text := ""
	k := testutils.RandInt(10, 200)

	for i := 0; i < k; i++ {
		text += random.String(uint8(testutils.RandInt(i, 245)))
	}

	user := &entity.User{
		ID:        testutils.RandInt64(9999999, 99999999),
		FirstName: random.String(19),
		LastName:  random.String(9),
	}

	now := time.Now()

	return &entity.Post{
		UserID:    user.ID,
		Name:      random.String(222),
		Image:     random.String(199),
		Text:      text,
		DTCreated: now,
		DTUpdated: now,
		Author: &entity.Author{
			ID:   user.ID,
			Name: user.FirstName + " " + user.LastName,
		},
	}
}
