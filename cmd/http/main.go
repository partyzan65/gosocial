package main

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/getsentry/sentry-go"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"

	"gitlab.com/partyzan65/gosocial/cmd/http/server"
)

var (
	cfg          = pflag.String("config", "./config.yaml", "config path")
	connLifetime = pflag.Duration("conn_lifetime", time.Second, "connection max lifetime")
	imgPath      = pflag.String("img_path", "./storage", "path for images")
)

func main() {
	pflag.Parse()

	config := viper.New()
	config.SetConfigType("yaml")

	if *cfg != "" {
		configFile, err := os.Open(*cfg)
		if err != nil {
			log.Printf("opening config failed: %s", err)
		}

		err = config.ReadConfig(configFile)
		if err != nil {
			log.Printf("reading config failed; %s", err)
		}
	}

	_ = config.BindPFlag("mysql.conn_lifetime", pflag.Lookup("conn_lifetime"))
	_ = config.BindPFlag("path.images", pflag.Lookup("img_path"))

	_ = config.BindEnv("db_driver", "DB_DRIVER")
	_ = config.BindEnv("mysql.master", "MYSQL_MASTER_DSN")
	_ = config.BindEnv("mysql.slave", "MYSQL_SLAVE_DSN")
	_ = config.BindEnv("mysql.proxy", "MYSQL_PROXY_DSN")
	//_ = config.BindEnv("postgres.dsn", "PG_DSN")
	_ = config.BindEnv("http.host", "HTTP_HOST")
	_ = config.BindEnv("http.port", "HTTP_PORT")
	_ = config.BindEnv("path.images", "IMG_PATH")
	_ = config.BindEnv("sentry.dsn", "SENTRY_DSN")

	if sentryDSN := config.GetString("sentry.dsn"); sentryDSN != "" {
		err := sentry.Init(sentry.ClientOptions{
			Dsn: sentryDSN,
		})
		if err != nil {
			log.Fatalf("sentry error: %s", err)
		}

		log.Print("Sentry initialized")
	}

	s, err := server.Create(config)
	if err != nil {
		sentry.CaptureException(err)
		log.Fatalf("create server error: %s", err)
	}

	addr := fmt.Sprintf("%s:%d", config.GetString("http.host"), config.GetInt("http.port"))
	if err := s.Start(addr); err != nil {
		sentry.CaptureException(err)
		s.Logger.Fatal(err)
	}
}
