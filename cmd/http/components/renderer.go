package components

import (
	"io"
	"reflect"

	"github.com/CloudyKit/jet"
	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
)

type jetRenderer struct {
	Views *jet.Set
}

var (
	valueOfFalse = reflect.ValueOf(false)
	valueOfTrue  = reflect.ValueOf(true)
)

func NewRenderer(path string) *jetRenderer {
	renderer := &jetRenderer{
		Views: jet.NewHTMLSet(path),
	}

	renderer.Views.AddGlobalFunc("isNil", func(arguments jet.Arguments) reflect.Value {
		if arguments.Get(0).IsNil() {
			return valueOfTrue
		}

		return valueOfFalse
	})

	return renderer
}

func (render *jetRenderer) Render(w io.Writer, name string, data interface{}, ctx echo.Context) error {
	view, err := render.Views.GetTemplate(name)
	if err != nil {
		return errors.Wrap(err, "getting template failed")
	}

	templateData, ok := data.(TemplateData)
	if !ok {
		return errors.New("data is not implements TemplateData interface")
	}

	err = view.Execute(w, templateData.JetVars(), templateData.Data())
	if err != nil {
		return errors.Wrap(err, "executing template failed")
	}

	return nil
}

type TemplateData interface {
	JetVars() jet.VarMap
	Data() map[string]interface{}
}
