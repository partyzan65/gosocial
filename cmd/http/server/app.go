package server

import (
	"database/sql"
	"gitlab.com/partyzan65/gosocial/cmd/http/handler/cdialog"
	"log"

	"github.com/getsentry/sentry-go"
	"github.com/jmoiron/sqlx"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
	"gitlab.com/partyzan65/gosocial/cmd/http/components"
	"gitlab.com/partyzan65/gosocial/cmd/http/handler/cauth"
	"gitlab.com/partyzan65/gosocial/cmd/http/handler/cimg"
	"gitlab.com/partyzan65/gosocial/cmd/http/handler/cimg/file"
	"gitlab.com/partyzan65/gosocial/cmd/http/handler/cuser"
	"gitlab.com/partyzan65/gosocial/pkg/auth"
	"gitlab.com/partyzan65/gosocial/pkg/executor"
	"gitlab.com/partyzan65/gosocial/pkg/user"

	_ "github.com/go-sql-driver/mysql"

	stupidrandom "github.com/partyzanex/stupid-random"
	errs "gitlab.com/partyzan65/gosocial/cmd/http/handler/cerr"
	authRepo "gitlab.com/partyzan65/gosocial/pkg/auth/repository/mysql"
	messageRepo "gitlab.com/partyzan65/gosocial/pkg/message/repository"
	regionRepo "gitlab.com/partyzan65/gosocial/pkg/region/repository"
	userRepo "gitlab.com/partyzan65/gosocial/pkg/user/repository/mysql"
)

func Create(config *viper.Viper) (e *echo.Echo, err error) {
	var slaves []*sqlx.DB

	master, err := sql.Open("mysql", config.GetString("mysql.master"))
	if err != nil {
		return nil, errors.Errorf("opening sql connection failed: %s", err)
	}
	err = master.Ping()
	if err != nil {
		log.Printf("ping to master failed: %s", err)
	}

	master.SetConnMaxLifetime(config.GetDuration("mysql.conn_lifetime"))
	master.SetMaxIdleConns(1000)
	master.SetMaxOpenConns(1000)

	dsnSlice := config.GetStringSlice("mysql.slaves")

	for _, dsn := range dsnSlice {
		slave, err := sqlx.Open("mysql", dsn)
		if err != nil {
			err := errors.Errorf("opening sql connection %s failed: %s", dsn, err)
			sentry.CaptureException(err)
			log.Println(err)
			continue
		}

		err = slave.Ping()
		if err != nil {
			err := errors.Errorf("ping to sql connection failed: %s", err)
			sentry.CaptureException(err)
			log.Println(err)
			continue
		}

		slave.SetConnMaxLifetime(config.GetDuration("mysql.conn_lifetime"))
		slave.SetMaxIdleConns(1000)
		slaves = append(slaves, slave)
	}

	proxy, err := sql.Open("mysql", config.GetString("mysql.proxy"))
	if err != nil {
		return nil, errors.Errorf("opening proxy connection failed: %s", err)
	}
	err = proxy.Ping()
	if err != nil {
		log.Printf("ping to proxy failed: %s", err)
	}

	users := userRepo.New(master)
	tokens := authRepo.New(master)
	regions := regionRepo.New(master)

	userCase := user.NewUseCase(users, regions)
	authCase := auth.NewUseCase(userCase, tokens)

	renderer := components.NewRenderer("./views")
	renderer.Views.SetDevelopmentMode(true)

	authHandler := cauth.Handler{
		Users:   userCase,
		Tokens:  authCase,
		Regions: regions,
		Views:   renderer.Views,
	}
	userHandler := cuser.New(userCase, authCase)

	imgHandler := cimg.Handler{
		Storage: file.New(config.GetString("path.images")),
	}

	messages := messageRepo.New(proxy)

	e = echo.New()
	e.Use(middleware.Recover())
	e.Use(middleware.Logger())
	e.Use(WithSentry)

	msgHandler := &cdialog.Handler{
		Users:    userCase,
		Messages: messages,
		Logger:   e.Logger,
	}

	if n := len(slaves); n > 0 {
		pgr := stupidrandom.New()
		for _, slave := range slaves {
			pgr.Add(slave, 1/float32(n))
		}

		e.Use(func(handlerFunc echo.HandlerFunc) echo.HandlerFunc {
			return func(ctx echo.Context) error {
				req := ctx.Request()
				ctx.SetRequest(req.WithContext(executor.WithExecutor(req.Context(), pgr.Get().(*sqlx.DB))))
				return handlerFunc(ctx)
			}
		})
	}

	e.File("/favicon.ico", "./assets/favicon.ico")
	assets := e.Group("/assets")
	assets.Static("/", "./assets")

	img := e.Group("/img")
	img.GET("/*", imgHandler.GetImage)

	e.Use(userHandler.AuthByCookie)

	e.Renderer = renderer
	e.HTTPErrorHandler = errs.ErrorHandler

	api := e.Group("/api", userHandler.AuthByToken)

	e.GET("/auth", authHandler.Auth, authHandler.RequiredNoAuth)
	e.GET("/register", authHandler.Register, authHandler.RequiredNoAuth)
	e.Any("/logout", userHandler.Logout, userHandler.AuthByCookie)

	api.POST("/auth", authHandler.AuthHandle)
	api.POST("/register", authHandler.RegisterHandle)

	e.GET("/", userHandler.List)
	e.GET("/user/:id", userHandler.Single)
	e.GET("/profile", userHandler.Profile, userHandler.RequiredAuthByCookie)

	//wr := msgHandler.WrapSocket()
	e.Any("/ws/messages/:user_id", msgHandler.WebSocket, userHandler.RequiredAuthByCookie)
	e.Any("/ws/messages/:user_id/refresh", msgHandler.WebSocket, userHandler.RequiredAuthByCookie)
	e.GET("/messages/:user_id", msgHandler.GetMessages, userHandler.RequiredAuthByCookie)
	api.POST("/profile/image", imgHandler.UploadUserImage, userHandler.RequiredAuthByCookie)

	return e, nil
}
