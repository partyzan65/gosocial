package server

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/getsentry/sentry-go"
	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
)

func WithSentry(handlerFunc echo.HandlerFunc) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		err := handlerFunc(ctx)
		if err != nil {
			var body, headers string
			if r := ctx.Request().Body; r != nil {
				b, er := ioutil.ReadAll(r)
				if er != nil {
					sentry.CaptureException(er)
				} else {
					body = string(b)
					defer func() {
						_ = r.Close()
					}()
				}
			}
			if h := ctx.Request().Header; h != nil {
				b, er := json.Marshal(h)
				if er != nil {
					sentry.CaptureException(er)
				} else {
					headers = string(b)
				}
			}
			exc := errors.Wrapf(err,
				"request %s %s with body %s and headers %s returns error",
				ctx.Request().Method, ctx.Request().URL.String(),
				body, headers,
			)

			switch e := err.(type) {
			case *echo.HTTPError:
				if e.Code != http.StatusNotFound {
					sentry.CaptureException(exc)
				}
			default:
				sentry.CaptureException(exc)
			}
		}

		return err
	}
}
