$(function () {
  var $message = $('#message-text');
  var $submit = $('#message-send');
  var $chat = $('#chat-content');
  $chat.scrollTop($chat.prop("scrollHeight"));
  var scrolled = false;

  var socket = new WebSocket("ws://" + window.location.host + "/ws/messages/" + $('#to-id').val());
  var interval = null;
  socket.onopen = function (ev) {
    console.log(ev);

    interval = setInterval(function () {
      var msg = {
        type: "refresh",
        data: ""
      };
      socket.send(JSON.stringify(msg))
    }, 1000)
  };

  socket.onclose = function (ev) {
    console.log(ev);
    clearInterval(interval);
  };

  socket.onmessage = function (ev) {
    var msg = JSON.parse(ev.data);
    switch (msg.type) {
      case "refresh":
        var messages = JSON.parse(msg.data);
        refresh(messages);
        break;
      default:
        console.log(msg);
        break;
    }
  };

  socket.onerror = function (ev) {
    console.log(ev);
  };

  $chat.on('scroll', function (e) {
    scrolled = true;
  });

  function sendMessage($el) {
    var text = $el.val();

    $el.val('');

    if (text.length === 0) {
      return
    }

    scrolled = false;
    var msg = {
      type: "send",
      data: JSON.stringify({
        to: parseInt($el.attr('data-to')),
        message: text
      })
    };
    socket.send(JSON.stringify(msg));
  }

  function formatDate(dt) {
    var day = dt.getDay() + 1;
    if (day < 10) {
      day = "0" + day
    }

    var month = dt.getMonth() + 1;
    if (month < 10) {
      month = "0" + month;
    }

    return month + '/' + day + '/' + dt.getFullYear() + ' ' + dt.getHours() + ':' + dt.getMinutes() + ':' + dt.getSeconds();
  }

  function refresh(messages) {

    var toID = parseInt($('#to-id').val());
    for (var i in messages) {
      var message = messages[i];
      message.dt_created = Math.floor(message.dt_created / 1000000);
      if ($chat.find('#message-' + message.to_id + '-' + message.dt_created).length === 0) {
        var html = '';
        if (toID === message.to_id) {
          html = '<div class="media media-chat media-chat-reverse" id="message-' + message.to_id + '-' + message.dt_created + '">'
        } else {
          html = '<div class="media media-chat" id="message-' + message.to_id + '-' + message.dt_created + '">'
        }
        html += '<div class="media-body"><p>' + message.message + '</p>';
        var dt = new Date();
        dt.setTime(message.dt_created);
        html += '<p class="meta"><time>' + formatDate(dt) + '</time></p>';
        html += '</div></div>';

        $chat.append(html);
      }
    }
    if (!scrolled) {
      $chat.scrollTop($chat.prop("scrollHeight"));
    }
  }

  $message.on('keyup', function (e) {
    e.preventDefault();
    e.stopPropagation();

    if (e.ctrlKey && e.keyCode === 13) {
      sendMessage($message);
    }
  });

  $submit.on('click', function (e) {
    e.preventDefault();
    e.stopPropagation();

    sendMessage($message);
  });
});

