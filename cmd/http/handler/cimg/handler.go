package cimg

import (
	"io"
	"time"
)

type Stat struct {
	LastModified time.Time
}

type Storage interface {
	Exists(name string) bool
	Submit(file io.Reader, name string) error
	Fetch(name string) (io.ReadCloser, *Stat, error)
}

type Handler struct {
	Storage Storage
}
