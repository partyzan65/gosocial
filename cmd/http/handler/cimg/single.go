package cimg

import (
	"bytes"
	"fmt"
	"image"
	"math"
	"net/http"
	"path/filepath"
	"strconv"
	"time"

	"github.com/disintegration/imaging"
	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
)

// todo: refactoring and/or remove
// bad code
func (h *Handler) GetImage(ctx echo.Context) (err error) {
	name := ctx.Param("*")

	var width, height int
	if _, exists := ctx.QueryParams()["w"]; exists {
		width, err = strconv.Atoi(ctx.QueryParam("w"))
		if err != nil {
			ctx.Logger().Errorf("bad width: %s", err)
			return ctx.NoContent(http.StatusBadRequest)
		}
	}

	if _, exists := ctx.QueryParams()["h"]; exists {
		height, err = strconv.Atoi(ctx.QueryParam("h"))
		if err != nil {
			ctx.Logger().Errorf("bad height: %s", err)
			return ctx.NoContent(http.StatusBadRequest)
		}
	}

	var img image.Image
	var dt time.Time
	fileName := name

	if width > 0 && height > 0 {
		fileName = fmt.Sprintf("%s/%d/%d/%s", filepath.Dir(name), width, height, filepath.Base(name))
		if !h.Storage.Exists(fileName) {
			if !h.Storage.Exists(name) {
				return ctx.Redirect(http.StatusFound, "/assets/svg/picture.svg")
			}

			file, stat, err := h.Storage.Fetch(name)
			if err != nil {
				return errors.Wrap(err, "fetching file from storage failed")
			}
			defer func() {
				err = file.Close()
			}()

			dt = stat.LastModified

			img, err = imaging.Decode(file)
			if err != nil {
				return errors.Wrap(err, "decoding file to image failed")
			}

			// crop and resize image
			widthReal := img.Bounds().Dx()
			widthScale := float64(widthReal) / float64(width)
			heightCrop := widthScale * float64(height)

			imgCropped := imaging.CropCenter(img, widthReal, int(math.Floor(heightCrop)))
			imgResized := imaging.Resize(imgCropped, width, height, imaging.Gaussian)

			fw := &bytes.Buffer{}
			err = imaging.Encode(fw, imgResized, imaging.JPEG, imaging.JPEGQuality(80))
			if err != nil {
				return errors.Wrap(err, "encoding image failed")
			}
			//go func() {
			//	err = h.Storage.Submit(fw, fileName)
			//	if err != nil {
			//		ctx.Logger().Error(errors.Wrap(err, "submitting image to storage failed"))
			//	}
			//}()

			img = imgResized
		} else {
			file, stat, err := h.Storage.Fetch(fileName)
			if err != nil {
				return errors.Wrap(err, "fetching file from storage failed")
			}
			defer func() {
				err = file.Close()
			}()

			dt = stat.LastModified

			img, err = imaging.Decode(file)
			if err != nil {
				return errors.Wrap(err, "decoding file to image failed")
			}
		}
	} else {
		if !h.Storage.Exists(fileName) {
			return ctx.Redirect(http.StatusFound, "/assets/svg/picture.svg")
		}

		file, stat, err := h.Storage.Fetch(fileName)
		if err != nil {
			return errors.Wrap(err, "fetching file from storage failed")
		}
		defer func() {
			err = file.Close()
		}()

		dt = stat.LastModified

		img, err = imaging.Decode(file)
		if err != nil {
			return errors.Wrap(err, "decoding file to image failed")
		}
	}

	buf := &bytes.Buffer{}

	err = imaging.Encode(buf, img, imaging.JPEG, imaging.JPEGQuality(80))
	if err != nil {
		return errors.Wrap(err, "encoding image failed")
	}

	ctx.Response().Header().Set("Accept-Ranges", "bytes")
	ctx.Response().Header().Set(echo.HeaderContentLength, strconv.Itoa(len(buf.Bytes())))
	ctx.Response().Header().Set(echo.HeaderLastModified, dt.Format("Mon, 2 Jan 2006 15:04:05 MST"))
	ctx.Response().Header().Set("Cache-Control", "max-age=86400, public")

	_, err = buf.WriteTo(ctx.Response())
	if err != nil {
		return errors.Wrap(err, "writing to response failed")
	}

	return
}
