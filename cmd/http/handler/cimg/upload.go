package cimg

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"gitlab.com/partyzan65/gosocial/cmd/http/components"
	"gitlab.com/partyzan65/gosocial/pkg/entity"
)

func (h *Handler) UploadUserImage(ctx echo.Context) error {
	user, ok := ctx.Get(entity.UserContextKey).(*entity.User)
	if !ok {
		return echo.NewHTTPError(http.StatusForbidden, "unauthorized user")
	}

	form, err := ctx.MultipartForm()
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	photos, ok := form.File["photo"]
	if !ok {
		return echo.NewHTTPError(http.StatusBadRequest, "no files")
	}

	photo := photos[0]

	src, err := photo.Open()
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, err)
	}
	defer func() {
		err := src.Close()
		if err != nil {
			ctx.Logger().Errorf("closing photo source failed: %s", err)
		}
	}()

	fileName := user.Avatar()

	err = h.Storage.Submit(src, fileName)
	if err != nil {
		return errors.Wrap(err, "submit photo to storage failed")
	}

	return ctx.JSON(http.StatusOK, &components.Response{
		Success: true,
		Data:    fileName,
	})
}
