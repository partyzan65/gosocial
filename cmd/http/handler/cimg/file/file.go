package file

import (
	"bytes"
	"image/jpeg"
	"image/png"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/h2non/filetype"
	"github.com/pkg/errors"
	"gitlab.com/partyzan65/gosocial/cmd/http/handler/cimg"
)

type Storage struct {
	path string
}

func (s *Storage) Exists(name string) bool {
	path := filepath.Join(s.path, name)
	_, err := os.Stat(path)
	return !os.IsNotExist(err)
}

func (s *Storage) Submit(file io.Reader, name string) (err error) {
	fileName := filepath.Join(s.path, name)
	filePath := filepath.Dir(fileName)
	if err := s.createPath(filePath); err != nil {
		return errors.Wrap(err, "creating path failed")
	}

	b, err := ioutil.ReadAll(file)
	if err != nil {
		return errors.Wrap(err, "reading form file failed")
	}

	t, err := filetype.Match(b)
	if err != nil {
		return errors.Wrap(err, "detecting file type failed")
	}
	if t == filetype.Unknown {
		return errors.Wrap(err, "unknown file type")
	}

	switch t.MIME.Value {
	case "image/jpeg":
	case "image/png":
		img, err := png.Decode(bytes.NewReader(b))
		if err != nil {
			return errors.Wrap(err, "decoding image/png failed")
		}

		buf := &bytes.Buffer{}
		err = jpeg.Encode(buf, img, &jpeg.Options{Quality: 67})
		if err != nil {
			return errors.Wrap(err, "encoding to jpeg failed")
		}

		b = buf.Bytes()
	default:
		return errors.New("unsupported file type")
	}

	f, err := os.Create(fileName)
	if err != nil {
		return errors.Wrap(err, "creating file failed")
	}
	defer func() {
		err = f.Close()
	}()

	_, err = f.Write(b)
	if err != nil {
		return errors.Wrap(err, "writing to file failed")
	}

	return
}

func (s *Storage) Fetch(name string) (io.ReadCloser, *cimg.Stat, error) {
	fileName := filepath.Join(s.path, name)

	file, err := os.Open(fileName)
	if os.IsNotExist(err) {
		return nil, nil, os.ErrNotExist
	}
	if err != nil {
		return nil, nil, errors.Wrap(err, "opening file failed")
	}

	info, err := file.Stat()
	if err != nil {
		return nil, nil, errors.Wrap(err, "getting file stat failed")
	}

	stat := &cimg.Stat{LastModified: info.ModTime()}

	return file, stat, nil
}

func (*Storage) createPath(path string) error {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		err := os.MkdirAll(path, os.ModePerm)
		if err != nil {
			return errors.Wrap(err, "creating directory failed")
		}
	}

	return nil
}

func New(path string) *Storage {
	return &Storage{
		path: path,
	}
}
