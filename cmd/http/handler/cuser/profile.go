package cuser

import (
	"github.com/pkg/errors"
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/partyzan65/gosocial/pkg/entity"
)

func (h *handler) Profile(ctx echo.Context) error {
	authUser := ctx.Get(entity.UserContextKey).(*entity.User)

	err := h.Users.BindRegions(ctx.Request().Context(), authUser)
	if err != nil {
		return errors.Wrap(err, "bing user region failed")
	}

	return ctx.Render(http.StatusOK, "user/profile.jet", &usersData{
		AuthUser: authUser,
		User:     authUser,
	})
}
