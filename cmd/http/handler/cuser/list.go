package cuser

import (
	"net/http"

	"github.com/CloudyKit/jet"
	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"gitlab.com/partyzan65/gosocial/cmd/http/components"
	"gitlab.com/partyzan65/gosocial/pkg/entity"
	"gitlab.com/partyzan65/gosocial/pkg/user"
)

type usersData struct {
	User, AuthUser *entity.User
	Users          []*entity.User
	Pagination     *components.Pagination
	Query          string
}

func (data *usersData) JetVars() jet.VarMap {
	vars := make(jet.VarMap)
	vars.Set("title", "GoSocial Users")
	vars.Set("query", data.Query)

	if data.User != nil {
		vars.Set("user", data.User)
	}
	if data.AuthUser != nil {
		vars.Set("authUser", data.AuthUser)
	}
	if data.Pagination != nil {
		vars.Set("pagination", data.Pagination)
	}

	return vars
}

func (data *usersData) Data() map[string]interface{} {
	dataMap := make(map[string]interface{})

	//if data.User != nil {
	//	dataMap["user"] = data.User
	//}

	if data.Users != nil {
		dataMap["users"] = data.Users
	}

	dataMap["count"] = len(data.Users)

	return dataMap
}

var (
	limit = 50
)

func (h *handler) List(ctx echo.Context) error {
	q := ctx.QueryParam("q")

	authUser, _ := ctx.Get(entity.UserContextKey).(*entity.User)

	nav := &components.Pagination{
		Ctx:         ctx,
		URLTemplate: "/?p={page}",
		PageParam:   "p",
	}

	if q != "" {
		nav.URLTemplate += "&q=" + q
	}

	nav.ParsePage()

	users, total, err := h.Users.Repository.Search(ctx.Request().Context(), &user.Filter{
		Name:   q,
		Limit:  limit,
		Offset: nav.Page*limit - limit,
	})
	if err != nil {
		return errors.Wrap(err, "search users failed")
	}

	err = h.Users.BindRegions(ctx.Request().Context(), users...)
	if err != nil {
		return errors.Wrap(err, "bing user region failed")
	}

	nav.Total = total
	nav.Limit = limit

	return ctx.Render(http.StatusOK, "user/index.jet", &usersData{
		AuthUser:   authUser,
		Users:      users,
		Pagination: nav,
		Query:      q,
	})
}
