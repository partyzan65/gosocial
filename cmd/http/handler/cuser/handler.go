package cuser

import (
	"gitlab.com/partyzan65/gosocial/pkg/auth"
	"gitlab.com/partyzan65/gosocial/pkg/user"
)

type handler struct {
	Users  *user.UseCase
	Tokens *auth.UseCase
}

func New(users *user.UseCase, auth *auth.UseCase) *handler {
	return &handler{
		Users:  users,
		Tokens: auth,
	}
}
