package cuser

import (
	"net/http"
	"time"

	"github.com/labstack/echo/v4"
	"gitlab.com/partyzan65/gosocial/pkg/entity"
)

func (h *handler) Logout(ctx echo.Context) error {
	if user, ok := ctx.Get(entity.UserContextKey).(*entity.User); ok && user != nil {
		ctx.SetCookie(&http.Cookie{
			Name:    "access_token",
			Expires: time.Now().Add(-24 * time.Hour),
			Domain:  ctx.Request().Host,
		})
	}

	return ctx.Redirect(http.StatusFound, "/")
}
