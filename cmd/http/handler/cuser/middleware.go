package cuser

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"gitlab.com/partyzan65/gosocial/pkg/entity"
)

func (h *handler) AuthByToken(handlerFunc echo.HandlerFunc) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		return handlerFunc(ctx)
	}
}

func (h *handler) RequiredAuthByCookie(handlerFunc echo.HandlerFunc) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		if user, ok := ctx.Get(entity.UserContextKey).(*entity.User); ok && user != nil {
			return handlerFunc(ctx)
		}

		tc, err := ctx.Cookie("access_token")
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err)
		}

		c := ctx.Request().Context()
		accessToken, err := h.Tokens.SearchToken(c, tc.Value)
		if err == entity.ErrTokenNotFound {
			return echo.NewHTTPError(http.StatusNotFound, err)
		}
		if err != nil {
			return errors.Wrap(err, "search token failed")
		}

		if accessToken.Type != entity.AuthToken {
			return echo.NewHTTPError(http.StatusNotAcceptable)
		}

		if accessToken.IsExpired() {
			return echo.NewHTTPError(http.StatusGone, "access token has expired")
		}

		accessToken.User.Current = true
		ctx.Set(entity.UserContextKey, accessToken.User)

		return handlerFunc(ctx)
	}
}

func (h *handler) AuthByCookie(handlerFunc echo.HandlerFunc) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		tc, err := ctx.Cookie("access_token")
		if err != nil {
			return handlerFunc(ctx)
		}

		c := ctx.Request().Context()
		accessToken, err := h.Tokens.SearchToken(c, tc.Value)
		if err != nil {
			return handlerFunc(ctx)
		}

		if accessToken.Type != entity.AuthToken {
			return handlerFunc(ctx)
		}

		if accessToken.IsExpired() {
			return handlerFunc(ctx)
		}

		accessToken.User.Current = true
		ctx.Set(entity.UserContextKey, accessToken.User)

		return handlerFunc(ctx)
	}
}
