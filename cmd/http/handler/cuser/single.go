package cuser

import (
	"log"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"gitlab.com/partyzan65/gosocial/pkg/entity"
)

func (h *handler) Single(ctx echo.Context) error {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 64)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err)
	}

	user, err := h.Users.SearchByID(ctx.Request().Context(), id)
	if err != nil {
		return errors.Wrap(err, "getting user by id failed")
	}

	authUser, ok := ctx.Get(entity.UserContextKey).(*entity.User)

	if ok && authUser.ID == user.ID {
		user.Current = true
	}

	err = h.Users.BindRegions(ctx.Request().Context(), user, authUser)
	if err != nil {
		return errors.Wrap(err, "bing user region failed")
	}

	log.Println(user.Region)

	return ctx.Render(http.StatusOK, "user/profile.jet", &usersData{
		User:     user,
		AuthUser: authUser,
	})
}
