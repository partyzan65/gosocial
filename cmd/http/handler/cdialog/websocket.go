package cdialog

import (
	"encoding/json"
	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"gitlab.com/partyzan65/gosocial/pkg/datetime"
	"gitlab.com/partyzan65/gosocial/pkg/entity"
	"gitlab.com/partyzan65/gosocial/pkg/message"
	"golang.org/x/net/websocket"
	"io"
	"net/http"
	"strconv"
	"time"
)

func (h *Handler) WebSocket(ctx echo.Context) error {
	user, ok := ctx.Get(entity.UserContextKey).(*entity.User)
	if !ok || user == nil {
		return echo.NewHTTPError(http.StatusUnauthorized)
	}

	toID, err := strconv.ParseInt(ctx.Param("user_id"), 10, 64)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error()).SetInternal(err)
	}

	to, err := h.Users.SearchByID(ctx.Request().Context(), toID)
	if err != nil {
		return errors.Wrap(err, "search user by id failed")
	}

	websocket.Handler(func(ws *websocket.Conn) {
		defer func() {
			_ = ws.Close()
		}()

		for {
			wsmsg := &wsMessage{}
			err := websocket.JSON.Receive(ws, wsmsg)
			if err != nil && err == io.EOF {
				return
			}
			if err != nil {
				ctx.Logger().Error(err)
				continue
			}

			switch wsmsg.Type {
			case "send":
				m := &rawMessage{}
				err = json.Unmarshal([]byte(wsmsg.Data), m)
				if err != nil {
					ctx.Logger().Error(err)
					continue
				}

				mes := &entity.Message{
					UserID:    uint64(user.ID),
					FromID:    uint64(user.ID),
					ToID:      uint64(to.ID),
					RegionID:  to.RegionID,
					DTCreated: uint64(time.Now().UnixNano()),
					HasRead:   false,
					Text:      m.Message,
					DT:        datetime.Date(time.Now()),
					From:      user,
					To:        to,
				}

				err = h.Messages.Create(ctx.Request().Context(), mes)
				if err != nil {
					ctx.Logger().Errorf("create message failed: %s", err)
					continue
				}
			case "refresh":
				messages, err := h.Messages.Messages(ctx.Request().Context(), message.Filter{
					UserID: uint64(user.ID),
					FromID: uint64(to.ID),
					Limit:  10,
					Order:  "dt_created desc",
				})
				if err != nil {
					ctx.Logger().Error(err)
					continue
				}

				b, err := json.Marshal(messages)
				if err != nil {
					ctx.Logger().Error(err)
					continue
				}

				//d, err := json.Marshal()
				//if err != nil {
				//	ctx.Logger().Error(err)
				//	continue
				//}

				err = websocket.JSON.Send(ws, wsMessage{
					Type: "refresh",
					Data: string(b),
				})
				if err != nil {
					ctx.Logger().Error(err)
					continue
				}
			}
		}
	}).ServeHTTP(ctx.Response(), ctx.Request())

	return nil
}

type wsMessage struct {
	Type string `json:"type"`
	Data string `json:"data"`
}

type rawMessage struct {
	To      int64  `json:"to"`
	Message string `json:"message"`
}
