package cdialog

import (
	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"gitlab.com/partyzan65/gosocial/pkg/entity"
	"gitlab.com/partyzan65/gosocial/pkg/message"
	"net/http"
	"strconv"
)

func (h *Handler) GetMessages(ctx echo.Context) error {
	from := ctx.Get(entity.UserContextKey).(*entity.User)

	userID, err := strconv.ParseInt(ctx.Param("user_id"), 10, 64)
	if err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error()).SetInternal(err)
	}

	c := ctx.Request().Context()

	to, err := h.Users.SearchByID(c, userID)
	if err != nil {
		return errors.Wrap(err, "search user failed")
	}

	messages, err := h.Messages.Messages(c, message.Filter{
		UserID: uint64(from.ID),
		FromID: uint64(to.ID),
		Limit:  1000,
		Offset: 0,
	})
	if err != nil {
		return errors.Wrap(err, "get messages failed")
	}

	return ctx.Render(http.StatusOK, "dialog/messages", &data{
		Messages: messages,
		From:     from,
		To:       to,
	})
}

//func (h *Handler) SendMessage(ctx echo.Context) error {
//
//}
