package cdialog

import (
	"github.com/CloudyKit/jet"
	"github.com/labstack/echo/v4"
	"gitlab.com/partyzan65/gosocial/pkg/entity"
	"gitlab.com/partyzan65/gosocial/pkg/message"
	"gitlab.com/partyzan65/gosocial/pkg/user"
)

type Handler struct {
	Users    *user.UseCase
	Messages message.Repository
	Logger   echo.Logger
}

type data struct {
	Messages entity.Messages
	From, To *entity.User
}

func (d *data) JetVars() jet.VarMap {
	vars := make(jet.VarMap)
	vars.Set("messages", d.Messages)
	vars.Set("authUser", d.From)

	return vars
}

func (d *data) Data() map[string]interface{} {
	dataMap := make(map[string]interface{})

	dataMap["From"] = d.From
	dataMap["To"] = d.To

	return dataMap
}
