package cnews

import (
	"gitlab.com/partyzan65/gosocial/pkg/feed"
	"gitlab.com/partyzan65/gosocial/pkg/post"
)

type Handler struct {
	Posts post.Repository
	Feed  feed.Repository
}
