package cerr

import (
	"net/http"
	"strings"

	"github.com/CloudyKit/jet"
	"github.com/labstack/echo/v4"
	"gitlab.com/partyzan65/gosocial/cmd/http/components"
)

func ErrorHandler(e error, ctx echo.Context) {
	if strings.Contains(ctx.Path(), "/api") {
		JSONError(e, ctx)
		return
	}

	HTMLError(e, ctx)
}

type htmlData struct {
	Code  int
	Title string
	Error string
}

func (data htmlData) JetVars() jet.VarMap {
	vars := make(jet.VarMap)
	vars.Set("code", data.Code)
	vars.Set("error", data.Error)
	vars.Set("title", data.Title)

	return vars
}

func (htmlData) Data() map[string]interface{} {
	return nil
}

func HTMLError(e error, ctx echo.Context) {
	code := http.StatusInternalServerError
	title := ""
	if he, ok := e.(*echo.HTTPError); ok {
		code = he.Code

		if he.Internal != nil {
			title = he.Internal.Error()
		} else {
			switch code {
			case http.StatusBadRequest:
				title = "Bad Request"
			case http.StatusInternalServerError:
				title = "Internal Server Error"
			case http.StatusNotFound:
				title = "Not Found"
			}
		}
	}

	data := &htmlData{
		Code:  code,
		Title: title,
		Error: e.Error(),
	}

	err := ctx.Render(code, "error/error.jet", data)
	if err != nil {
		ctx.Logger().Error(err)
	}

	ctx.Logger().Error(e)
}

func JSONError(e error, ctx echo.Context) {
	code := http.StatusInternalServerError
	if he, ok := e.(*echo.HTTPError); ok {
		code = he.Code
	}

	resp := &components.Response{
		Success: false,
		Error:   e.Error(),
	}

	if err := ctx.JSON(code, resp); err != nil {
		ctx.Logger().Error(err)
	}

	ctx.Logger().Error(e)
}
