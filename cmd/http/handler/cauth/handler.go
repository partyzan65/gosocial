package cauth

import (
	"net/http"

	"github.com/CloudyKit/jet"
	"github.com/labstack/echo/v4"
	"gitlab.com/partyzan65/gosocial/pkg/auth"
	"gitlab.com/partyzan65/gosocial/pkg/entity"
	"gitlab.com/partyzan65/gosocial/pkg/region"
	"gitlab.com/partyzan65/gosocial/pkg/user"
)

type Handler struct {
	Users   *user.UseCase
	Tokens  *auth.UseCase
	Regions region.Repository

	Views *jet.Set
}

func (Handler) RequiredNoAuth(handlerFunc echo.HandlerFunc) echo.HandlerFunc {
	return func(ctx echo.Context) error {
		if u, ok := ctx.Get(entity.UserContextKey).(*entity.User); ok && u != nil {
			return ctx.Redirect(http.StatusFound, "/")
		}

		return handlerFunc(ctx)
	}
}
