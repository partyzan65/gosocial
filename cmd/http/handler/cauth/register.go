package cauth

import (
	"net/http"

	"github.com/CloudyKit/jet"
	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"gitlab.com/partyzan65/gosocial/cmd/http/components"
	"gitlab.com/partyzan65/gosocial/pkg/entity"
)

type registerData struct {
	Regions []*entity.Region
}

func (data *registerData) JetVars() jet.VarMap {
	vars := make(jet.VarMap)
	vars.Set("title", "Register")
	vars.Set("regions", data.Regions)

	return vars
}

func (registerData) Data() map[string]interface{} {
	return nil
}

func (h *Handler) Register(ctx echo.Context) error {
	if user, ok := ctx.Get(entity.UserContextKey).(*entity.User); ok && user != nil {
		return ctx.Redirect(http.StatusFound, "/")
	}

	regions, err := h.Regions.Search(ctx.Request().Context())
	if err != nil {
		return errors.Wrap(err, "get regions failed")
	}

	return ctx.Render(http.StatusOK, "auth/register.jet", &registerData{
		Regions: regions,
	})
}

func (h *Handler) RegisterHandle(ctx echo.Context) error {
	req := entity.User{}

	if err := ctx.Bind(&req); err != nil {
		return echo.NewHTTPError(
			http.StatusBadRequest,
			errors.Wrap(err, "binding json failed"),
		)
	}

	req.Status = entity.UserNew

	err := h.Users.Register(ctx.Request().Context(), &req)
	if err != nil {
		return errors.Wrap(err, "creating user failed")
	}

	return ctx.JSON(http.StatusOK, components.Response{
		Success: true,
	})
}
