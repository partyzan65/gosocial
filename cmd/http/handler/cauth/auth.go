package cauth

import (
	"net/http"
	"time"

	"github.com/CloudyKit/jet"
	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"gitlab.com/partyzan65/gosocial/cmd/http/components"
	"gitlab.com/partyzan65/gosocial/pkg/user"
)

type authData struct{}

func (authData) JetVars() jet.VarMap {
	vars := make(jet.VarMap)
	vars.Set("title", "Sign In")

	return vars
}

func (authData) Data() map[string]interface{} {
	return nil
}

func (h *Handler) Auth(ctx echo.Context) error {
	return ctx.Render(http.StatusOK, "auth/login.jet", &authData{})
}

type authRequest struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}

type authResponse struct {
	AccessToken string    `json:"access_token"`
	DTExpired   time.Time `json:"dt_expired"`
}

func (h *Handler) AuthHandle(ctx echo.Context) error {
	req := &authRequest{}

	if err := ctx.Bind(&req); err != nil {
		return echo.NewHTTPError(
			http.StatusBadRequest,
			errors.Wrap(err, "binding json failed"),
		)
	}

	c := ctx.Request().Context()

	u, err := h.Users.SearchByLogin(c, req.Login)
	if err == user.ErrUserNotFound {
		return echo.NewHTTPError(http.StatusNotFound, err)
	}
	if err != nil {
		return errors.Wrap(err, "search user by login failed")
	}

	result, err := h.Users.ComparePassword(u, req.Password)
	if err != nil {
		return errors.Wrap(err, "compare password failed")
	}
	if !result {
		return echo.NewHTTPError(http.StatusForbidden, errors.New("invalid password"))
	}

	token, err := h.Tokens.CreateAuthToken(c, u)
	if err != nil {
		return errors.Wrap(err, "creating auth token failed")
	}

	err = h.Users.SetLastLogged(c, u)
	if err != nil {
		return errors.Wrap(err, "updating user failed")
	}

	http.SetCookie(ctx.Response(), &http.Cookie{
		Name:    "access_token",
		Value:   token.Token,
		Expires: token.DTExpired,
	})

	return ctx.JSON(http.StatusOK, &components.Response{
		Success: true,
		Data: &authResponse{
			AccessToken: token.Token,
			DTExpired:   token.DTExpired,
		},
	})
}
