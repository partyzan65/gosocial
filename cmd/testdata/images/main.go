package main

import (
	"bytes"
	"compress/gzip"
	"crypto/sha1"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"github.com/disintegration/imaging"
	"github.com/pkg/errors"
	"github.com/spf13/pflag"
	"image"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"
)

var (
	count   = pflag.Int("c", 1, "")
	streams = pflag.Int("s", 1, "")
	out     = pflag.String("o", "", "output dir")
)

func main() {
	pflag.Parse()

	if *out == "*" {
		log.Fatal("required flag --o")
	}

	for i := 1; i <= *count; i++ {
		wg := sync.WaitGroup{}
		wg.Add(*streams)

		for j := 0; j < *streams; j++ {
			go func() {
				defer wg.Done()

				img, name, err := RandomImage()
				if err != nil {
					log.Println("getting random image failed:", err)
					return
				}

				err = SaveImage(*out, name, img)
				if err != nil {
					log.Println("saving random image failed:", err)
					return
				}
			}()
		}
		wg.Wait()

		time.Sleep(500 * time.Millisecond)
		log.Printf("Finished %d of %d", i, *count)
	}
}

func hash(b []byte) string {
	h := sha1.Sum(b)
	return hex.EncodeToString(h[:])
}

func SaveImage(dir, name string, img image.Image) error {
	path, err := os.Getwd()
	if err != nil {
		return err
	}

	fileName := fmt.Sprintf("%s/%s/%s", path, dir, name)

	err = imaging.Save(img, fileName)
	if err != nil {
		return errors.Wrap(err, "saving image failed")
	}

	return nil
}

func RandomImage() (image.Image, string, error) {
	payload := strings.NewReader("method=getImage")

	req, err := http.NewRequest("POST", "http://rand.by/ajax.php", payload)
	if err != nil {
		return nil, "", errors.Wrap(err, "creating request failed")
	}

	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/77.0.3865.90 Chrome/77.0.3865.90 Safari/537.36")
	req.Header.Add("Accept", "*/*")
	req.Header.Add("Cache-Control", "no-cache")
	req.Header.Add("Host", "rand.by")
	req.Header.Add("Accept-Encoding", "gzip, deflate")
	req.Header.Add("Content-Length", "15")
	req.Header.Add("Connection", "keep-alive")
	req.Header.Add("cache-control", "no-cache")

	cl := &http.Client{}
	res, err := cl.Do(req)
	if err != nil {
		return nil, "", errors.Wrap(err, "executing request failed")
	}
	defer func() {
		_ = res.Body.Close()
	}()

	gz, err := gzip.NewReader(res.Body)
	if err != nil {
		return nil, "", errors.Wrap(err, "decode from gzip failed")
	}

	body, err := ioutil.ReadAll(gz)
	if err != nil {
		return nil, "", errors.Wrap(err, "reading request failed")
	}

	rb := bytes.Replace(body, []byte("\\"), nil, -1)
	result := &struct {
		Image string `json:"image"`
	}{}

	err = json.Unmarshal(rb, result)
	if err != nil {
		return nil, "", errors.Wrap(err, "decode from json failed")
	}

	img, err := cl.Get(result.Image)
	if err != nil {
		return nil, "", errors.Wrap(err, "downloading image failed")
	}
	defer func() {
		_ = img.Body.Close()
	}()

	b, err := ioutil.ReadAll(img.Body)
	if err != nil {
		return nil, "", errors.Wrap(err, "reading image failed")
	}

	i, ext, err := image.Decode(bytes.NewReader(b))
	if err != nil {
		return nil, "", errors.Wrap(err, "decoding image failed")
	}

	name := fmt.Sprintf("%s.%s", hash(b), ext)

	return i, name, nil
}
