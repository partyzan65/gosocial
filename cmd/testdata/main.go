package main

import (
	"context"
	"database/sql"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"

	astistring "github.com/asticode/go-astitools/string"
	"github.com/essentialkaos/translit"
	"github.com/partyzanex/testutils"
	"github.com/spf13/pflag"
	"gitlab.com/partyzan65/gosocial/pkg/datetime"
	"gitlab.com/partyzan65/gosocial/pkg/entity"
	"gitlab.com/partyzan65/gosocial/pkg/user"
	"gitlab.com/partyzan65/gosocial/pkg/user/repository/postgres"
)

var (
	lang         = pflag.String("lang", "", "language set (ru or en)")
	gender       = pflag.String("gender", "", "gander (male or female)")
	rawUrl       = pflag.String("url", "http://localhost:5500", "target base url")
	streams      = pflag.Int("s", 1, "")
	out          = pflag.String("o", "./users.txt", "target file with users login and passwords")
	mysqlDSN     = pflag.String("mysql", "root:535353@tcp(127.0.0.1:3306)/social?loc=Europe%2FMoscow&parseTime=true", "mysql dsn")
	connLifetime = pflag.Duration("conn_lifetime", time.Second, "mysql dsn")
)

func main() {
	pflag.Parse()

	if *lang == "" {
		log.Fatal("required flag --lang")
	}

	//uri, err := url.Parse(*rawUrl)
	//if err != nil {
	//	log.Fatalf("parsing url %s failed: %s", *rawUrl, err)
	//}

	users, err := Generate(*lang, *gender)
	if err != nil {
		log.Fatalf("generating users failed: %s", err)
	}

	//c := client.Client{
	//	Client:  &http.Client{},
	//	BaseURL: uri,
	//}

	db, err := sql.Open("postgres", *mysqlDSN)
	if err != nil {
		log.Fatalf("opening sql connection failed: %s", err)
	}
	db.SetConnMaxLifetime(*connLifetime)
	db.SetMaxIdleConns(1000)

	repo := postgres.New(db)
	userUseCase := user.NewUseCase(repo)
	//img := file.New("./storage")

	res, err := os.OpenFile(*out, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
	if err != nil {
		log.Fatalf("creating result file failed: %s", err)
	}
	defer func() {
		_ = res.Close()
	}()

	wg := &sync.WaitGroup{}
	wg.Add(*streams)

	i, n := 0, len(users)
	for _, user := range users {
		//user := user
		go func(user entity.User) {
			defer wg.Done()

			err := userUseCase.Register(context.Background(), &user)
			if err != nil {
				log.Println(err)
				return
			}

			//
			//	err := c.Register(user)
			//	if err != nil {
			//		log.Println(err)
			//		return
			//	}
			//
			//	auth, err := c.Login(user)
			//	if err != nil {
			//		log.Println(err)
			//		return
			//	}

			//
			//	err = auth.UploadPhoto(img)
			//	_ = img.Close()
			//	if err != nil {
			//		log.Println(err)
			//		goto Image
			//	}
			//
			//Write:
			//	_, err = res.WriteString(fmt.Sprintf("%s:%s\n", user.Login, user.Password))
			//	if err != nil {
			//		log.Println(err)
			//		goto Write
			//	}
		}(user)

		i++
		if i%*streams == 0 {
			wg.Wait()

			log.Printf("Finished %d of %d users", i, n)

			wg.Add(*streams)
		}
	}
}

var (
	images []os.FileInfo
	imx    sync.Mutex
)

func GetRandomImage(gender string) (io.ReadCloser, error) {
	path, err := os.Getwd()
	if err != nil {
		return nil, err
	}

	dir := fmt.Sprintf("%s/cmd/testdata/data/images/%s/", path, gender)

	if images == nil {
		files, err := ioutil.ReadDir(dir)
		if err != nil {
			return nil, err
		}

		for _, file := range files {
			if !strings.Contains(file.Name(), ".jpeg") {
				continue
			}

			images = append(images, file)
		}
	}

	n := len(images) - 1

	imx.Lock()
	defer imx.Unlock()

Random:
	i := testutils.RandInt(0, n)
	if images[i] == nil {
		goto Random
	}

	fi, err := os.Open(filepath.Join(dir, images[i].Name()))
	if err != nil {
		return nil, err
	}

	return fi, nil
}

func Generate(lang, gender string) (map[string]entity.User, error) {
	firstNames, err := ReadSet("names_"+gender+"_"+lang, "\n")
	if err != nil {
		return nil, fmt.Errorf("reading first names set failed: %s", err)
	}

	lastNames, err := ReadSet("last_names_"+lang, "\n")
	if err != nil {
		return nil, fmt.Errorf("reading last names set failed: %s", err)
	}

	desc, err := ReadSet("desc_"+lang, "\n--------------------------\n")
	if err != nil {
		return nil, fmt.Errorf("reading descriptions set failed: %s", err)
	}

	cities, err := ReadSet("cities_"+lang, "\n")
	if err != nil {
		return nil, fmt.Errorf("reading cities set failed: %s", err)
	}

	users := make(map[string]entity.User, len(firstNames)*len(lastNames))

	d, c := len(desc)-1, len(cities)-1
	DTMin := time.Now().AddDate(testutils.RandInt(-45, -20), testutils.RandInt(-12, 0), testutils.RandInt(-45, -20))
	DTMax := time.Now().AddDate(-16, 0, 0)
	for _, firstName := range firstNames {
		ci := testutils.RandInt(0, c)
		for _, lastName := range lastNames {
			if gender == string(entity.Female) {
				lastName += "а"
			}
			login := fmt.Sprintf("%s%s%d@cart-one.ru", translit.EncodeToICAO(lastName), translit.EncodeToICAO(firstName), testutils.RandInt(1970, 2005))
			users[login] = entity.User{
				Login:       strings.ToLower(login),
				Password:    astistring.RandomString(testutils.RandInt(6, 12)),
				FirstName:   lastName,
				LastName:    firstName,
				BDate:       datetime.Date(testutils.RandomDate(DTMin, DTMax)),
				Gender:      entity.Gender(gender),
				Description: desc[d],
				City:        cities[ci],
				Status:      entity.UserNew,
			}
			d--
			if d == 0 {
				d = len(desc) - 1
			}
		}
	}

	return users, nil
}

func ReadSet(name string, sep string) ([]string, error) {
	dir, err := os.Getwd()
	if err != nil {
		return nil, err
	}

	fileName := fmt.Sprintf("%s/cmd/testdata/data/%s.txt", dir, name)

	b, err := ioutil.ReadFile(fileName)
	if err != nil {
		return nil, err
	}

	return strings.Split(string(b), sep), nil
}
