package client

import (
	"bytes"
	"encoding/json"
	"io"
	"mime/multipart"
	"net/http"
	"net/url"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"gitlab.com/partyzan65/gosocial/cmd/http/components"
	"gitlab.com/partyzan65/gosocial/pkg/entity"
)

type AuthorizedUser struct {
	*entity.User

	Client      *Client
	AccessToken string
	DTExpired   time.Time
}

func (au *AuthorizedUser) UploadPhoto(r io.Reader) error {
	buf := &bytes.Buffer{}

	formData := multipart.NewWriter(buf)
	//defer func() {
	//	_ = formData.Close()
	//}()

	w, err := formData.CreateFormFile("photo", "base.jpg")
	if err != nil {
		return errors.Wrap(err, "creating form file filed")
	}

	_, err = io.Copy(w, r)
	if err != nil {
		return errors.Wrap(err, "copying filed")
	}
	_ = formData.Close()

	uri := *au.Client.BaseURL
	uri.Path = "/api/profile/image"

	req, err := http.NewRequest(http.MethodPost, uri.String(), buf)
	if err != nil {
		return errors.Wrap(err, "creating request failed")
	}

	req.Header.Add(echo.HeaderContentType, formData.FormDataContentType())

	req.AddCookie(&http.Cookie{
		Name:    "access_token",
		Value:   au.AccessToken,
		Expires: au.DTExpired,
	})

	resp, err := au.Client.Do(req)
	if err != nil {
		return errors.Wrap(err, "executing request failed")
	}
	defer func() {
		_ = resp.Body.Close()
	}()

	if resp.StatusCode != http.StatusOK {
		return errors.Errorf("uploading failed with code %d", resp.StatusCode)
	}

	result := &components.Response{}

	err = json.NewDecoder(resp.Body).Decode(result)
	if err != nil {
		return errors.Wrap(err, "decoding response failed")
	}

	if !result.Success {
		return errors.Errorf("server returns error: %s", result.Error)
	}

	return nil
}

type Client struct {
	*http.Client

	BaseURL *url.URL
}

func (c *Client) Register(user entity.User) error {
	uri := *c.BaseURL
	uri.Path = "/api/register"

	b, err := json.Marshal(user)
	if err != nil {
		return errors.Wrap(err, "marshaling user failed")
	}

	req, err := http.NewRequest(http.MethodPost, uri.String(), bytes.NewReader(b))
	if err != nil {
		return errors.Wrap(err, "creating request failed")
	}

	req.Header.Add(echo.HeaderContentType, echo.MIMEApplicationJSON)

	resp, err := c.Do(req)
	if err != nil {
		return errors.Wrap(err, "executing request failed")
	}

	if resp.StatusCode != http.StatusOK {
		return errors.Errorf("registration failed with code %d", resp.StatusCode)
	}

	resp.Cookies()

	return nil
}

func (c *Client) Login(user entity.User) (*AuthorizedUser, error) {
	uri := *c.BaseURL
	uri.Path = "/api/auth"

	auth := &struct {
		Login    string `json:"login"`
		Password string `json:"password"`
	}{
		Login:    user.Login,
		Password: user.Password,
	}

	b, err := json.Marshal(auth)
	if err != nil {
		return nil, errors.Wrap(err, "marshaling auth request failed")
	}

	req, err := http.NewRequest(http.MethodPost, uri.String(), bytes.NewReader(b))
	if err != nil {
		return nil, errors.Wrap(err, "creating request failed")
	}

	req.Header.Add(echo.HeaderContentType, echo.MIMEApplicationJSON)

	resp, err := c.Do(req)
	if err != nil {
		return nil, errors.Wrap(err, "executing request failed")
	}
	defer func() {
		_ = resp.Body.Close()
	}()

	if resp.StatusCode != http.StatusOK {
		return nil, errors.Errorf("login failed with code %d", resp.StatusCode)
	}

	result := &struct {
		*components.Response
		Data *struct {
			AccessToken string    `json:"access_token"`
			DTExpired   time.Time `json:"dt_expired"`
		} `json:"data"`
	}{}

	err = json.NewDecoder(resp.Body).Decode(result)
	if err != nil {
		return nil, errors.Wrap(err, "decoding response failed")
	}

	return &AuthorizedUser{
		User:        &user,
		AccessToken: result.Data.AccessToken,
		DTExpired:   result.Data.DTExpired,
		Client:      c,
	}, nil
}
