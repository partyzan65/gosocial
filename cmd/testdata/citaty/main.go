package main

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/spf13/pflag"
	"log"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"
)

var (
	count   = pflag.Int("c", 1, "")
	streams = pflag.Int("s", 1, "")
	out     = pflag.String("o", "", "output file")
)

func main() {
	pflag.Parse()

	var items []string
	mx := sync.Mutex{}

	for i := 1; i <= *count; i++ {
		wg := sync.WaitGroup{}
		wg.Add(*streams)

		for j := 0; j < *streams; j++ {
			go func() {
				defer wg.Done()

				str, err := Do()
				if err != nil {
					log.Println(err)
					return
				}

				mx.Lock()
				items = append(items, str)
				mx.Unlock()
			}()
		}

		wg.Wait()
		log.Printf("Finished %d of %d", i, *count)
	}

	str := strings.Join(items, "\n--------------------------\n")

	file, err := os.OpenFile(*out, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
	if err != nil {
		log.Fatalf("creating file failed: %s", err)
	}
	defer file.Close()

	_, err = file.WriteString(str)
	if err != nil {
		log.Fatalf("writing to file failed: %s", err)
	}
}

const URL = "https://finewords.ru/sluchajnaya?_=%d"

func Do() (string, error) {
	url := fmt.Sprintf(URL, time.Now().Unix())
	resp, err := http.Get(url)
	if err != nil {
		return "", err
	}
	defer func() {
		_ = resp.Body.Close()
	}()

	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		return "", err
	}

	return doc.Text(), nil
}
