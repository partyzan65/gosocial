FROM golang:1.13.7-alpine as builder

RUN apk add --no-cache openssh-client git

COPY . /go/src/gitlab.com/patyzan65/gosocial
WORKDIR /go/src/gitlab.com/patyzan65/gosocial

ARG ssh_key

ENV CGO_ENABLED 0
ENV GOPRIVATE gitlab.com
RUN mkdir -p /root/.ssh && \
  chmod 700 /root/.ssh && \
  echo "StrictHostKeyChecking no " > /root/.ssh/config && \
  chmod 600 /root/.ssh/config && \
  echo "$ssh_key" > /root/.ssh/id_rsa && \
  chmod 600 /root/.ssh/id_rsa && \
  git config --global url."git@gitlab.com:".insteadOf https://gitlab.com/
RUN go build -v -o /app ./cmd/http/main.go



FROM alpine:3.11

EXPOSE 9900

WORKDIR /app

COPY --from=builder /usr/local/go/lib/time/zoneinfo.zip /usr/local/go/lib/time/zoneinfo.zip
COPY --from=builder /app /app/app
COPY --from=builder /go/src/gitlab.com/patyzan65/gosocial/cmd/http/assets /app/assets
COPY --from=builder /go/src/gitlab.com/patyzan65/gosocial/cmd/http/views /app/views

RUN apk --update add tzdata ca-certificates && \
  cp /usr/share/zoneinfo/Europe/Moscow /etc/localtime && \
  echo "Europe/Moscow" > /etc/timezone && \
  date && \
  apk del tzdata

ENTRYPOINT ["./app"]
