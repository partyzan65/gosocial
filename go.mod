module gitlab.com/partyzan65/gosocial

go 1.13

require (
	github.com/CloudyKit/fastprinter v0.0.0-20170127035650-74b38d55f37a // indirect
	github.com/CloudyKit/jet v2.1.2+incompatible
	github.com/asticode/go-astitools v1.2.0
	github.com/disintegration/imaging v1.6.1
	github.com/getsentry/sentry-go v0.3.0
	github.com/go-redis/redis/v7 v7.2.0
	github.com/go-sql-driver/mysql v1.4.1
	github.com/googollee/go-engine.io v1.4.2
	github.com/googollee/go-socket.io v1.4.2
	github.com/h2non/filetype v1.0.10
	github.com/jmoiron/sqlx v1.2.0
	github.com/labstack/echo/v4 v4.1.14
	github.com/labstack/gommon v0.3.0
	github.com/mattn/go-sqlite3 v1.10.0 // indirect
	github.com/partyzanex/stupid-random v1.0.0
	github.com/partyzanex/testutils v0.0.3
	github.com/pkg/errors v0.8.1
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.3.2
	github.com/tarantool/go-tarantool v0.0.0-20191229181800-f4ece3508d87
	golang.org/x/crypto v0.0.0-20191227163750-53104e6ec876
	golang.org/x/net v0.0.0-20191209160850-c0dbc17a3553
	gopkg.in/vmihailenco/msgpack.v2 v2.9.1 // indirect
)
